
export function viewport(
    bounds: [number, number, number, number],
    viewport: [number, number]
): { zoom: number }

export function bounds(
    center: [number, number] | { lon: number, lng: number },
    zoom: number,
    dimensions: [number, number],
    tileSize?: number
): any