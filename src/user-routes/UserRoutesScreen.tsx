import React, { PureComponent } from 'react';
import ContainerView from '../layout/ContainerView';
import { Text, FlatList, View, Image, Dimensions } from 'react-native';
import CardHeader from '../material/CardHeader';
import MenuButton from '../menu/MenuButton';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import { ActionButton } from 'react-native-material-ui';
import CreateUserRouteDialog from './CreateUserRouteDialog';
import UserRoutesList from './UserRoutesList';
const header = require('../../assets/user-routes-header.png');

const { width, height } = Dimensions.get('window');


interface State {
    dialogOpen: boolean
}

class UserRoutesScreen extends PureComponent<any, State> {

    state = {
        dialogOpen: false
    }

    render() {
        return (
            <ContainerView>
                <TravellirDrawer />
                <CreateUserRouteDialog open={this.state.dialogOpen} onRequestClose={() => this.setState({ dialogOpen: false })} />
                <FlatList data={[1]} keyExtractor={(item, i) => `user-route-screen-${i}`} renderItem={() =>
                    <View style={{ minHeight: height, paddingBottom: 10 }}>
                        <MenuButton />
                        <Image source={header} style={{ width, height: 200 }} />
                        <CardHeader text='Twoje trasy' />
                        <UserRoutesList />
                    </View>
                } />
                <ActionButton
                    onPress={() => this.setState({ dialogOpen: !this.state.dialogOpen })}
                    style={{ container: { backgroundColor: '#2A7EE0', width: 50, zIndex: 0, height: 50, elevation: 0 } }} />
            </ContainerView>
        )
    }
};

export default UserRoutesScreen;