
import { Region } from 'react-native-maps';
import { RawAction, TravellirModule } from '../travellir-redux';
import { UserRoute } from './user-routes-types';
import { LocationListItem } from '../travellir-map/MapTypes';


export interface UserRoutesState {
    userRoutes: {
        loading: boolean,
        routes: UserRoute[],
    }
    currentRoute: {
        route: UserRoute | null,
        requestedRouteId: string | null,
        locations: LocationListItem[]
    }
}

export const types = {
    USER_ROUTES_FETCHED: 'USER_ROUTES_FETCHED',
    REQUEST_CURRENT_ROUTE: 'REQUEST_CURRENT_ROUTE',
    CURRENT_ROUTE_FETCHED: 'CURRENT_ROUTE_FETCHED',
    CURRENT_ROUTE_LOCATIONS_FETCHED: 'CURRENT_ROUTE_LOCATIONS_FETCHED'
}

export const actions = {
    currentRouteLocationFetched(locations: LocationListItem[]): RawAction<LocationListItem[]> {
        return {
            type: types.CURRENT_ROUTE_LOCATIONS_FETCHED,
            payload: locations
        }
    },
    routesFetched(routes: UserRoute[]): RawAction<UserRoute[]> {
        return {
            type: types.USER_ROUTES_FETCHED,
            payload: routes
        }
    },
    requestCurrentRoute(routeId: string): RawAction<string> {
        return {
            type: types.REQUEST_CURRENT_ROUTE,
            payload: routeId
        }
    },
    currentRouteFetched(route: UserRoute) {
        return {
            type: types.CURRENT_ROUTE_FETCHED,
            payload: route
        }
    }
}

export const UserRoutesModule = {
    initialState: {
        currentRoute: {
            requestedRouteId: null,
            route: null,
            locations: []
        },
        userRoutes: {
            loading: false,
            routes: []
        }
    },
    reducers: {
        [types.USER_ROUTES_FETCHED](state: UserRoutesState, payload: UserRoute[]) {
            return {
                userRoutes: {
                    loading: false,
                    routes: payload
                }
            }
        },
        [types.REQUEST_CURRENT_ROUTE](state: UserRoutesState, routeId: string) {
            return {
                currentRoute: {
                    route: null,
                    locations: [],
                    requestedRouteId: routeId,
                }
            }
        },
        [types.CURRENT_ROUTE_FETCHED](state: UserRoutesState, route: UserRoute) {
            return {
                currentRoute: Object.assign({}, state.currentRoute, {
                    route
                })
            }
        },
        [types.CURRENT_ROUTE_LOCATIONS_FETCHED](state: UserRoutesState, locations: LocationListItem[]) {
            return {
                currentRoute: Object.assign({}, state.currentRoute, {
                    locations
                })
            }
        }
    }
} as TravellirModule<UserRoutesState>;
