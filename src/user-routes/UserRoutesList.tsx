import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { UserRoutesState } from './user-routes-redux';
import { connectState } from '../travellir-redux';
import { UserRoute } from './user-routes-types';
import CardListItem from '../material/CardListItem';
import { COLOR, Icon } from 'react-native-material-ui';
import { RippleFeedback } from 'react-native-material-ui';
import NavigatorService from '../navigation/NavigatorService';
import UserRoutesService from './UserRoutesService';
import RouteIcon from './RouteIcon';

class UserRoutesList extends Component<UserRoutesState> {
    render() {
        const { userRoutes } = this.props;
        return (
            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                {
                    userRoutes.routes.map((route, i) =>
                        <UserRouteCard route={route} key={`user-route-${i}`} />)
                }
            </View>
        )
    }
};

interface RouteCardProps {
    route: UserRoute
}
class UserRouteCard extends React.Component<RouteCardProps> {
    render() {
        const { route } = this.props;
        return (
            <CardListItem onPress={this.navigateToRoute} rippleColor={COLOR.blue200} accessible={true}>
                <RouteIcon locationIds={route.locationIds} />
                <View style={{ paddingVertical: 10 }}>
                    <Text style={{ fontSize: 20, marginBottom: 5, marginLeft: 3 }}>{route.name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="place" size={20} color={COLOR.blue400} />
                        <Text style={{ fontSize: 18, color: COLOR.grey600 }}>{route.locationIds.length}</Text>
                    </View>
                </View>
            </CardListItem>
        )
    }

    navigateToRoute = async () => {
        const { route } = this.props;
        UserRoutesService.requestCurrentRoute(route);
        NavigatorService.navigateTo('UserRouteScreen');
    }
}

export default connectState(UserRoutesList);