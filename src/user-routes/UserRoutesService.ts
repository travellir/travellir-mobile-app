import { AsyncStorage } from 'react-native';
import { UserRoute } from './user-routes-types';
import TravellirStore from '../TravellirStore';
import { actions } from './user-routes-redux';
import { uuid } from '../uuid/UUID';
import { LocationListItem } from '../travellir-map/MapTypes';
import Requests from '../http/Requests';

const userRoutesKey = '@travellir/user-routes';


async function readRoutes(): Promise<UserRoute[]> {
    const routesItem = await AsyncStorage.getItem(userRoutesKey);
    return JSON.parse(routesItem) as UserRoute[] || [];
};

async function fetchRoutes() {
    const routes = await readRoutes();
    TravellirStore.dispatch(actions.routesFetched(routes))
};

fetchRoutes();

export default {
    async changeRouteName(route: UserRoute, name: string, routes: UserRoute[]) {
        const updatedRoute = Object.assign({}, route, { name });
        this.currentRouteFetched(updatedRoute);

        const index: number = routes.filter(_route => _route.id === route.id)
            .map((_route, i) => i)[0];
        const updatedRoutes = routes.slice();
        updatedRoutes[index] = updatedRoute;

        AsyncStorage.setItem(userRoutesKey, JSON.stringify(updatedRoutes))
        TravellirStore.dispatch(actions.routesFetched(updatedRoutes))
    },
    async deleteRoute(route: UserRoute, routes: UserRoute[]) {
        const updatedRoutes = routes.filter(_route => _route.id !== route.id);

        AsyncStorage.setItem(userRoutesKey, JSON.stringify(updatedRoutes));
        TravellirStore.dispatch(actions.routesFetched(updatedRoutes));
    },
    readRoutes,
    fetchRoutes,

    async fetchLocations(route: UserRoute) {
        if (route.locationIds.length === 0)
            return;

        const parameters = route.locationIds
            .map(locationId => `locationIds=${locationId}`)
            .join('&')

        const locations = (await Requests.jsonGet('/api/locations?' + parameters)) as Array<LocationListItem>;
        TravellirStore.dispatch(actions.currentRouteLocationFetched(locations));
    },

    currentRouteFetched(route: UserRoute) {
        TravellirStore.dispatch(actions.currentRouteFetched(route))
    },

    requestCurrentRoute(route: UserRoute) {
        TravellirStore.dispatch(actions.requestCurrentRoute(route.id))
    },

    async saveAssignments(locationId: number, assignments: string[], routes: UserRoute[]) {
        const updatedRoutes = routes.slice();
        for (const route of routes) {
            if (assignments.includes(route.id) && !route.locationIds.includes(locationId)) {
                route.locationIds = route.locationIds.concat([locationId])
                continue;
            }
            if (!assignments.includes(route.id) && route.locationIds.includes(locationId)) {
                route.locationIds = route.locationIds.filter(_locationId => _locationId !== locationId);
                continue;
            }
        }
        AsyncStorage.setItem(userRoutesKey, JSON.stringify(updatedRoutes));
        TravellirStore.dispatch(actions.routesFetched(updatedRoutes));
        return updatedRoutes;
    },

    async createRoute(routeName: string) {
        const route: UserRoute = {
            id: uuid(),
            name: routeName,
            locationIds: []
        }

        const routes = this.readRoutes();
        const updatedRoutes = [route].concat(await routes);
        return AsyncStorage.setItem(userRoutesKey, JSON.stringify(updatedRoutes));
    },

    async saveRoute(route: UserRoute, routes: UserRoute[]) {
        const updatedRoutes = [route].concat(routes.filter(_route => _route.id !== route.id));

        await AsyncStorage.setItem(userRoutesKey, JSON.stringify(updatedRoutes));
        TravellirStore.dispatch(actions.routesFetched(updatedRoutes));
    }
}