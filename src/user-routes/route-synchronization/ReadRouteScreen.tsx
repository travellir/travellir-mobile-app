import * as React from 'react';
import { View, StyleSheet, Text, Image, Dimensions, ToastAndroid } from 'react-native';
import ContainerView from '../../layout/ContainerView';
import { UserRoute } from '../user-routes-types';
import { connectState } from '../../travellir-redux';
import { UserRoutesState } from '../user-routes-redux';
import NFCManager from 'react-native-nfc-manager';
import NfcNotSupportedAlert from './NfcNotSupportedAlert';
import NfcNotEnabledAlert from './NfcNotEnabledAlert';
import NfcEnabledPanel from './NfcEnabledPanel';
import UserRoutesService from '../UserRoutesService';
import NavigatorService from '../../navigation/NavigatorService';


const ScreenHeaderImage = require('../../../assets/route-synchronization-header.png');

interface Props {
}

interface State {
    nfcSupported: boolean,
    nfcEnabled: boolean,
    listenerRegistered: boolean
}

class ReadRouteScreen extends React.PureComponent<Props & UserRoutesState, State> {

    private interval: NodeJS.Timer | null = null;

    state = {
        nfcSupported: false,
        nfcEnabled: false,
        listenerRegistered: false
    }

    render() {
        const { currentRoute } = this.props;
        const { nfcSupported, nfcEnabled, listenerRegistered } = this.state;
        return (
            <ContainerView>
                <ScreenHeader />
                <View style={{ padding: 15 }}>
                    <Text style={{ fontSize: 16 }}>Twoja trasa zostanie odczytana z innego urządzenia przy użyciu funkcji NFC.</Text>
                </View>
                {!nfcSupported && <NfcNotSupportedAlert />}
                {!nfcEnabled && <NfcNotEnabledAlert />}
                {listenerRegistered && <NfcEnabledPanel />}
            </ContainerView>
        );
    }

    async componentDidUpdate(prevProps: any, prevState: State) {
        const state = this.state;
        if (!prevState.nfcEnabled && state.nfcEnabled) {
            if (!state.listenerRegistered) {
                await NFCManager.start();
                NFCManager.registerTagEvent(tag => {
                    if (tag.ndefMessage.length !== 2)
                        return;
                    const record1Payload = tag.ndefMessage[0].payload_str;
                    const record2Type = tag.ndefMessage[1].type_str;
                    const record2Payload = tag.ndefMessage[1].payload_str;
                    if (record1Payload === 'com.travellir' || record2Type === 'application/json') {
                        this.synchronizeMap(JSON.parse(record2Payload))
                    }
                });
                this.setState({
                    listenerRegistered: true
                })
            }
        }

        if (prevState.nfcEnabled && !state.nfcEnabled) {
            if (this.interval) {
                clearInterval(this.interval);
            }
            await NFCManager.unregisterTagEvent();
            NFCManager.stop();
            this.setState({ listenerRegistered: false })
        }
    }

    checkNfcStatus = async () => {
        const nfcSupported = await NFCManager.isSupported();
        this.setState({ nfcSupported });

        if (!nfcSupported)
            return;

        const nfcEnabled = await NFCManager.isEnabled();
        this.setState({ nfcEnabled });
    }

    async componentDidMount() {
        await this.checkNfcStatus();
        this.interval = setInterval(() => this.checkNfcStatus(), 500);
    }

    synchronizeMap = async (route: UserRoute) => {
        const { userRoutes } = this.props;
        ToastAndroid.show('Trasa została zsynchronizowana', 600)
        await UserRoutesService.saveRoute(route, userRoutes.routes);
        NavigatorService.goBack();
    }

    async componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
        const { listenerRegistered, nfcEnabled } = this.state;
        if (nfcEnabled) {
            await NFCManager.unregisterTagEvent();
            NFCManager.stop();
        }
    }

}

const ScreenHeader = () => (
    <View>
        <Image source={ScreenHeaderImage} style={{ height: 200 }} />
        <View style={{ position: 'absolute', padding: 15, bottom: 0, backgroundColor: 'rgba(0,0,0,0.7)', width: Dimensions.get('window').width }}>
            <Text style={{ fontSize: 18, color: '#d6d6d6' }}>Synchronizacja nowej trasy</Text>
        </View>
    </View>
);

export default connectState(ReadRouteScreen);