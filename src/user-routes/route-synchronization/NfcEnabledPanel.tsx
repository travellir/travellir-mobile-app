import * as React from 'react';
import { View, StyleSheet, Text, Image, } from 'react-native';

const NfcEnabledImage = require('../../../assets/nfc-enabled.png')

interface Props {
}

class NfcEnabledPanel extends React.PureComponent<Props, any> {
    render() {
        return (
            <View style={{ padding: 15 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', paddingBottom: 30, paddingTop: 0 }}>
                    <Image source={NfcEnabledImage} style={{ height: 140, width: 120 }} />
                </View>
                <Text style={{ fontSize: 16 }}>
                    NFC jest aktywne. Możesz teraz zbliżyć telefony.
                </Text>
            </View>
        );
    }
}

export default NfcEnabledPanel;