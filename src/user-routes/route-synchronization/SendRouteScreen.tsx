import * as React from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native';
import ContainerView from '../../layout/ContainerView';
import { UserRoute } from '../user-routes-types';
import { connectState } from '../../travellir-redux';
import { UserRoutesState } from '../user-routes-redux';
import NFCManager from 'react-native-nfc-manager';
import NfcNotSupportedAlert from './NfcNotSupportedAlert';
import NfcNotEnabledAlert from './NfcNotEnabledAlert';
import NfcEnabledPanel from './NfcEnabledPanel';


const ScreenHeaderImage = require('../../../assets/route-synchronization-header.png');

interface Props {
}

interface State {
    nfcSupported: boolean,
    nfcEnabled: boolean,
    tagWritten: boolean
}

class SendRouteScreen extends React.PureComponent<Props & UserRoutesState, State> {

    state = {
        nfcSupported: false,
        nfcEnabled: false,
        tagWritten: false
    }

    private interval: NodeJS.Timer | null = null;

    render() {
        const { currentRoute } = this.props;
        const { nfcSupported, nfcEnabled, tagWritten } = this.state;
        if (!currentRoute.route)
            return null;
        return (
            <ContainerView>
                <ScreenHeader route={currentRoute.route} />
                <View style={{ padding: 15 }}>
                    <Text style={{ fontSize: 16 }}>Twoja trasa zostanie udostępniona innemu urządzeniu przy użyciu funkcji NFC.</Text>
                </View>
                {!nfcSupported && <NfcNotSupportedAlert />}
                {!nfcEnabled && <NfcNotEnabledAlert />}
                {tagWritten && <NfcEnabledPanel />}
            </ContainerView>
        );
    }

    checkNfcStatus = async () => {
        const nfcSupported = await NFCManager.isSupported();
        this.setState({ nfcSupported });

        if (!nfcSupported)
            return;

        const nfcEnabled = await NFCManager.isEnabled();
        this.setState({ nfcEnabled });
    }

    async componentDidMount() {
        await this.checkNfcStatus();
        this.interval = setInterval(() => this.checkNfcStatus(), 500);
    }

    async componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
        const { tagWritten, nfcEnabled } = this.state;
        if (tagWritten)
            await NFCManager.cancelNdefWrite();
        if (nfcEnabled) {
            await NFCManager.unregisterTagEvent();
            NFCManager.stop();
        }
    }

    async componentDidUpdate(prevProps: any, prevState: State) {
        const props = this.props;
        const state = this.state;
        if (!prevState.nfcEnabled && state.nfcEnabled) {
            if (!state.tagWritten) {
                await NFCManager.start();
                await NFCManager.registerTagEvent(tag => {
                    // ?
                });

                NFCManager.requestNdefWrite(JSON.stringify(props.currentRoute.route), {
                    mime: 'application/json',
                    application: 'com.travellir'

                })
                this.setState({
                    tagWritten: true
                })
            }
        }

        if (prevState.nfcEnabled && !state.nfcEnabled) {
            if (this.interval) {
                clearInterval(this.interval);
            }
            await NFCManager.unregisterTagEvent();
            NFCManager.stop();
            this.setState({ tagWritten: false })
        }
    }

}

interface HeaderProps {
    route: UserRoute
}
const ScreenHeader = (props: HeaderProps) => {
    const { route } = props;
    return (
        <View>
            <Image source={ScreenHeaderImage} style={{ height: 200 }} />
            <View style={{ position: 'absolute', padding: 15, bottom: 0, backgroundColor: 'rgba(0,0,0,0.7)', width: Dimensions.get('window').width }}>
                <Text style={{ fontSize: 18, color: '#d6d6d6' }}>Udostępniasz trasę <Text style={{ fontWeight: 'bold' }}>{route.name}</Text></Text>
            </View>
        </View>
    )
}

export default connectState(SendRouteScreen);