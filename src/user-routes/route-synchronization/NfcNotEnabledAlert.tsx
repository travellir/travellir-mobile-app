import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { Card, Icon, COLOR, Button } from 'react-native-material-ui';
import NFCManager from 'react-native-nfc-manager';

interface Props {
}

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5
    }
})

class NfcNotEnabledAlert extends React.PureComponent<Props, any> {
    render() {
        return (
            <React.Fragment>
                <Card>
                    <View style={styles.content}>
                        <Icon style={{ marginHorizontal: 5 }} name="warning" color={COLOR.yellow600} />
                        <Text>Funkcja NFC jest wyłączona.</Text>
                    </View>
                </Card>
                <View style={{ paddingHorizontal: 8, paddingVertical: 5 }}>

                    <Button onPress={this.goToNFCSetting} raised={false} text={'Włącz funkcję NFC'} style={{
                        container: {
                            backgroundColor: COLOR.blue100,
                        },
                        text: {
                            color: COLOR.blue800
                        }
                    }} />
                </View>
            </React.Fragment>
        );
    }

    goToNFCSetting = () => {
        NFCManager.goToNfcSetting();
    }
}

export default NfcNotEnabledAlert;