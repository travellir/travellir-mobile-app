import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { Card, Icon, COLOR } from 'react-native-material-ui';

interface Props {
}

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5
    }
})

class NfcNotSupportedAlert extends React.PureComponent<Props, any> {
    render() {
        return (
            <Card>
                <View style={styles.content}>
                    <Icon style={{ marginHorizontal: 5 }} name="error" color={COLOR.red700} />
                    <Text>Twój telefon nie jest wyposażony w NFC.</Text>
                </View>
            </Card>
        );
    }
}
export default NfcNotSupportedAlert;