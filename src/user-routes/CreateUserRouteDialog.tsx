import React, { Component } from 'react';
import MaterialDialog from '../dialogs/MaterialDialog';
import { Text, View, Image, TextInput, StyleSheet } from 'react-native';
import { Icon } from 'react-native-material-ui';
import { RippleFeedback } from 'react-native-material-ui';
import { COLOR } from 'react-native-material-ui';
import { Button } from 'react-native-material-ui';
import UserRoutesService from './UserRoutesService';
import NavigatorService from '../navigation/NavigatorService';

const dialogHeader = require('../../assets/routes-header.png')

interface Props {
    open: boolean,
    onRequestClose: () => void
}

export interface FormErrors {
    [key: string]: string
}

interface State {
    card: number,
    routeName: string,
    errors: FormErrors
}

const styles = StyleSheet.create({
    textInput: {
        height: 50,
        padding: 5,
        backgroundColor: "#fff",
        borderRadius: 2,
        fontSize: 20,
        width: "100%",
        marginBottom: 20,
    },
});
class CreateUserRouteDialog extends Component<Props, State> {

    private dialog: MaterialDialog | null = null;

    constructor(props: Props) {
        super(props);
        this.state = {
            card: 0,
            routeName: '',
            errors: {}
        }
    }

    render() {
        const { card, errors } = this.state;

        return (
            <MaterialDialog
                ref={(ref) => { this.dialog = ref }}
                open={this.props.open}
                onRequestClose={this.props.onRequestClose}>
                <View>
                    <Image borderTopLeftRadius={2} borderTopRightRadius={2} source={dialogHeader} />
                    <View style={{ position: 'absolute', bottom: 0, padding: 10 }}>
                        <Text style={{ fontSize: 26, color: '#fff' }}>Nowa trasa</Text>
                    </View>
                </View>
                {card === 0 && (
                    <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <RouteCreationOption onPress={this.showForm} icon='add' label='Stwórz nową' />
                        <RouteCreationOption onPress={async () => this.navigateTo('ReadRouteScreen')} icon='sync' label='Synchronizuj' />
                    </View>
                )}
                {card === 1 && (
                    <View style={{ padding: 10, flexDirection: 'column', }}>
                        {
                            errors.routeName && <Text style={{ color: COLOR.red700, fontSize: 12 }}>{errors.routeName}</Text>
                        }
                        <TextInput
                            maxLength={50}
                            accessibilityLabel={'Podaj nazwę trasy'}
                            placeholder={'Podaj nazwę trasy'}
                            underlineColorAndroid="#2A7EE0"
                            onChangeText={(routeName) => this.setState({ routeName })}
                            secureTextEntry={false}
                            style={styles.textInput} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Button onPress={this.close} style={{ text: { color: '#2A7EE0' } }} text={'Zamknij'} />
                            <Button onPress={this.createRoute} style={{ text: { color: '#2A7EE0' } }} text={'Zapisz'} />
                        </View>
                    </View>
                )}
            </MaterialDialog>
        )
    }

    navigateTo = (screen: string) => {
        NavigatorService.navigateTo(screen);
        this.close();
    }

    close = () => {
        this.setState({
            card: 0,
            routeName: ''
        })
        if (this.dialog) {
            this.dialog._close();
        }
    }

    createRoute = async () => {
        const { routeName } = this.state;
        if (routeName.trim().length < 3) {
            this.setState({
                errors: {
                    routeName: 'Nazwa trasy musi być dłuższa niż 3 znaki'
                }
            })
        }
        else {
            this.setState({ errors: {} })
            const result = await UserRoutesService.createRoute(routeName);
            this.close();
            UserRoutesService.fetchRoutes();
        }
    }

    showForm = () => {
        this.setState({ card: 1 })
    }
};

interface RouteCreationOptionProps {
    icon: string,
    label: string,
    onPress?: () => void
}
class RouteCreationOption extends React.Component<RouteCreationOptionProps> {
    render() {
        const { icon, label, onPress } = this.props;
        return (
            <RippleFeedback onPress={onPress} color={COLOR.lightBlueA100} borderless={true}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <Icon name={icon} color="#6EA3FA" size={40} />
                    <Text style={{ fontSize: 18, marginTop: 5 }}>{label}</Text>
                </View>
            </RippleFeedback>
        );
    }
}

export default CreateUserRouteDialog;