import React, { Component } from 'react';
import { View, Image, Dimensions, Text, ActivityIndicator, ToastAndroid } from 'react-native';
import ListScreen from '../../layout/ListScreen';
import TravellirDrawer from '../../travellir-drawer/TravellirDrawer';
import CardHeader from '../../material/CardHeader';
import MenuButton from '../../menu/MenuButton';
import { UserRoutesState } from '../user-routes-redux';
import { UserRoute } from '../user-routes-types';
import UserRoutesService from '../UserRoutesService';
import { connectState } from '../../travellir-redux';
import RouteLocations from './RouteLocations';
import RouteMenu from './RouteMenu';
import { MenuProvider } from 'react-native-popup-menu';
import ChangeRouteNameDialog from './ChangeRouteNameDialog';
import DeleteRouteDialog from './DeleteRouteDialog';
import NavigatorService from '../../navigation/NavigatorService';
import { COLOR } from 'react-native-material-ui';

const { width, height } = Dimensions.get('window');
const header = require('../../../assets/route-screen-header.png')

interface State {
    changeNameDialogOpen: boolean,
    deleteRouteDialogOpen: boolean,
    locationsLoading: boolean
}


class UserRouteScreen extends React.PureComponent<UserRoutesState, State> {

    state = {
        changeNameDialogOpen: false,
        deleteRouteDialogOpen: false,
        locationsLoading: true
    }

    render() {
        const { currentRoute } = this.props;
        const { locationsLoading } = this.state;
        if (!currentRoute.route)
            return null; // todo fallback (loading?)

        return (
            <MenuProvider>
                <ChangeRouteNameDialog
                    route={currentRoute.route}
                    onRequestClose={() => this.setState({ changeNameDialogOpen: false })}
                    open={this.state.changeNameDialogOpen} />
                <DeleteRouteDialog
                    route={currentRoute.route}
                    open={this.state.deleteRouteDialogOpen}
                    onRequestClose={() => this.setState({ deleteRouteDialogOpen: false })} />
                <ListScreen
                    outerElements={<TravellirDrawer />}>
                    <View>
                        <MenuButton />
                        <RouteMenu
                            onShare={this.checkNotEmpty(() => NavigatorService.navigateTo('SendRouteScreen'))}
                            onShowMap={this.checkNotEmpty(() => NavigatorService.navigateTo('CustomMapScreen'))}
                            onDeleteRouteSelected={() => this.setState({ deleteRouteDialogOpen: true })}
                            onChangeNameSelected={() => this.setState({ changeNameDialogOpen: true })} />
                        <Image source={header} style={{ width, height: 200 }} />
                        <CardHeader>
                            <Text style={{ fontSize: 19 }}>Trasa <Text style={{ fontWeight: 'bold' }}>{currentRoute.route.name}</Text></Text>
                        </CardHeader>
                        {locationsLoading ? <ListLoader /> : <RouteLocations />}
                    </View>
                </ListScreen>
            </MenuProvider>
        )
    }

    checkNotEmpty = (func: () => void) => {
        const { currentRoute } = this.props;
        return () => {
            if (!currentRoute.route)
                return;
            if (currentRoute.locations.length === 0) {
                ToastAndroid.show('Nie można wykonać wskazanej operacji, ponieważ do trasy nie przypisano żadnej lokalizacji.', 1000);
            }
            else
                func();
        }
    }

    async componentDidMount() {
        const { currentRoute, userRoutes } = this.props;
        if (!currentRoute.requestedRouteId)
            return;
        const route: UserRoute | undefined = userRoutes.routes
            .find(route => route.id === currentRoute.requestedRouteId);

        if (route) {
            UserRoutesService.currentRouteFetched(route);
            this.setState({ locationsLoading: true })
            UserRoutesService.fetchLocations(route);
            this.setState({ locationsLoading: false })
        }
    }
};

const ListLoader = () => (
    <View style={{ flexDirection: 'row', marginTop: '10%', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <ActivityIndicator size={80} color={COLOR.blue500} />
    </View>
)

export default connectState(UserRouteScreen);