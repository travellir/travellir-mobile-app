import * as React from 'react';
import { View, StyleSheet, Text, TextInput, Dimensions, } from 'react-native';
import { DialogProps } from '../../dialogs/DialogProperties';
import { UserRoute } from '../user-routes-types';
import MaterialDialog from '../../dialogs/MaterialDialog';
import { COLOR, Button } from 'react-native-material-ui';
import ChangeRouteNameForm from './ChangeRouteNameForm';
import UserRoutesService from '../UserRoutesService';
import { UserRoutesState } from '../user-routes-redux';
import { connectState } from '../../travellir-redux';

export interface Props extends DialogProps, UserRoutesState {
    route: UserRoute
}

class ChangeRouteNameDialog extends React.PureComponent<Props> {

    render() {
        const { open, onRequestClose, route } = this.props;
        return (
            <MaterialDialog width={Dimensions.get('window').width * .9} open={open} onRequestClose={onRequestClose}>
                <ChangeRouteNameForm
                    route={route}
                    onSave={this.changeRouteName}
                    actions={saveAction => (
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                            <Button onPress={onRequestClose} style={{ text: { color: '#2A7EE0' } }} text={'Zamknij'} />
                            <Button onPress={saveAction} style={{ text: { color: '#2A7EE0' } }} text={'Zapisz'} />
                        </View>
                    )}
                />
            </MaterialDialog>
        );
    }


    changeRouteName = async (routeName: string) => {
        const { route, userRoutes, onRequestClose } = this.props;
        await UserRoutesService.changeRouteName(route, routeName, userRoutes.routes);
        onRequestClose();
    }
}


export default connectState(ChangeRouteNameDialog);