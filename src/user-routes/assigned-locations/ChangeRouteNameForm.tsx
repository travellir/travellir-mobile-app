import * as React from 'react';
import { View, StyleSheet, Text, TextInput, } from 'react-native';
import { UserRoute } from '../user-routes-types';
import { COLOR } from 'react-native-material-ui';

export interface Props {
    route: UserRoute,
    onSave: (routeName: string) => void,
    actions: (saveAction: () => void) => React.ReactNode
}

class ChangeRouteNameForm extends React.PureComponent<Props, any> {

    constructor(props: Props) {
        super(props);
        this.state = {
            errors: {},
            routeName: props.route.name
        }
    }

    render() {
        const { routeName, errors } = this.state;
        return (
            <View>
                <View style={{ padding: 15 }}>
                    {
                        errors.routeName && <Text style={{ color: COLOR.red700, fontSize: 12 }}>{errors.routeName}</Text>
                    }
                    <TextInput
                        maxLength={50}
                        placeholder={'Nowa nazwa trasy'}
                        underlineColorAndroid="#2A7EE0"
                        onChangeText={(name) => this.setState({ routeName: name })}
                        secureTextEntry={false}
                        value={this.state.routeName}
                        style={styles.textInput} />
                </View>
                {this.props.actions(this.onSave)}
            </View>
        );
    }

    onSave = () => {
        const { routeName } = this.state;
        if (routeName.trim().length < 3) {
            this.setState({
                errors: {
                    routeName: 'Nazwa trasy musi być dłuższa niż 3 znaki'
                }
            })
            return;
        }

        this.setState({ errors: {} });
        this.props.onSave(routeName);
    }


}

const styles = StyleSheet.create({
    textInput: {
        height: 50,
        padding: 5,
        backgroundColor: "#fff",
        borderRadius: 2,
        fontSize: 20,
        width: "100%",
        marginBottom: 20,
    },
});

export default ChangeRouteNameForm;