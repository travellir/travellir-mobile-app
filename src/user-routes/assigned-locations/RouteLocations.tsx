import React, { Component } from 'react';
import { View, Image, Text, Dimensions } from 'react-native';
import { connectState } from '../../travellir-redux';
import { UserRoutesState } from '../user-routes-redux';
import CardListItem from '../../material/CardListItem';
import { LocationListItem } from '../../travellir-map/MapTypes';
import { COLOR, IconToggle, Icon } from 'react-native-material-ui';
import { RippleFeedback } from 'react-native-material-ui';
import MarkersService from '../../travellir-map/MarkersService';
import NavigatorService from '../../navigation/NavigatorService';
import CalloutService from '../../travellir-map/callouts/CalloutService';
import LocationListCard from '../../material/LocationListCard';
import DistanceInfo from '../../localization/DistanceInfo';
import LocationPageService from '../../location-page/LocationPageService';

const { width } = Dimensions.get('window');

class RouteLocations extends Component<UserRoutesState> {
    render() {
        const { currentRoute } = this.props;
        return (
            <View style={{ flexDirection: 'column', alignItems: 'center', paddingBottom: 10 }}>
                {currentRoute.locations.map(location =>
                    <RouteLocationCard location={location} key={`route-location-${location.id}`} />
                )}
            </View>
        )
    }
};

interface Props {
    location: LocationListItem
}

class RouteLocationCard extends React.Component<Props> {
    render() {
        const { location } = this.props;
        return (
            <LocationListCard
                onPress={this.gotoLocationPage}
                location={location}
                rightElement={
                    <RippleFeedback onPress={this.showLocationOnMap} color={COLOR.orangeA100}>
                        <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                            <Icon name="map" color={COLOR.deepOrange600} size={25} />
                            <DistanceInfo coords={location.coords} />
                        </View>
                    </RippleFeedback>
                }
            />
        );
    }

    gotoLocationPage = async () => {
        const { location } = this.props;
        LocationPageService.openPage(location.id);
    }

    showLocationOnMap = async () => {
        const { location } = this.props;
        setTimeout(() => {
            MarkersService.panToLocation(location);
            CalloutService.toggleCallout(location.id);
            NavigatorService.navigateTo('TravellirMapScreen');
        }, 100)
    }
}

export default connectState(RouteLocations);