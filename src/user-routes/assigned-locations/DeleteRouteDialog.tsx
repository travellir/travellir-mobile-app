import * as React from 'react';
import { View, StyleSheet, Text, Dimensions, } from 'react-native';
import { DialogProps } from '../../dialogs/DialogProperties';
import { UserRoutesState } from '../user-routes-redux';
import MaterialDialog from '../../dialogs/MaterialDialog';
import { UserRoute } from '../user-routes-types';
import { Button, COLOR } from 'react-native-material-ui';
import { connectState } from '../../travellir-redux';
import UserRoutesService from '../UserRoutesService';
import NavigatorService from '../../navigation/NavigatorService';

export interface Props extends DialogProps, UserRoutesState {
    route: UserRoute
}

class DeleteRouteDialog extends React.PureComponent<Props> {
    render() {
        const { open, onRequestClose, route } = this.props;
        return (
            <MaterialDialog width={Dimensions.get('window').width * .9} open={open} onRequestClose={onRequestClose}>
                <View style={{ padding: 15 }}>
                    <Text style={{ fontSize: 20 }}>Na pewno chcesz skasować trasę <Text style={{ fontWeight: 'bold' }}>{route.name}</Text>?</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                    <Button onPress={onRequestClose} style={{ text: { color: '#2A7EE0' } }} text={'Zamknij'} />
                    <Button onPress={this.deleteRoute} style={{ text: { color: COLOR.red400 } }} text={'Kasuj'} />
                </View>
            </MaterialDialog>
        );
    }

    deleteRoute = async () => {
        const { route, userRoutes, onRequestClose } = this.props;
        await UserRoutesService.deleteRoute(route, userRoutes.routes);
        onRequestClose();
        NavigatorService.goBack();
    }
}

export default connectState(DeleteRouteDialog);