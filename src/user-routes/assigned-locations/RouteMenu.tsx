import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import OptionsMenu, { MenuOption } from '../../menu/OptionsMenu';

export interface RouteMenuProps {
    onChangeNameSelected: () => void
    onDeleteRouteSelected: () => void,
    onShowMap: () => void,
    onShare: () => void
}

class RouteMenu extends React.PureComponent<RouteMenuProps> {
    render() {
        const { onChangeNameSelected, onDeleteRouteSelected, onShowMap, onShare } = this.props;
        return (
            <OptionsMenu>
                <MenuOption onPress={onShowMap} icon='map' text="Wyświetl trasę" />
                <MenuOption onPress={onChangeNameSelected} icon="edit" text="Zmień nazwę" />
                <MenuOption onPress={onShare} icon="share" divider={true} text="Udostępnij" />
                <MenuOption onPress={onDeleteRouteSelected} icon="delete" text="Usuń" />
            </OptionsMenu>
        );
    }
}
export default RouteMenu;