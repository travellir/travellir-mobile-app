export interface UserRoute {
    id: string,
    name: string,
    locationIds: number[]
}