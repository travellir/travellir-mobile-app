import * as React from 'react';
import { View, StyleSheet, Text, Image, } from 'react-native';
import { Icon, COLOR } from 'react-native-material-ui';

export interface Props {
    locationIds: number[]
}

class RouteIcon extends React.PureComponent<Props> {
    render() {
        const { locationIds } = this.props;
        if (locationIds.length === 0) {
            return (
                <View style={{ width: 75, flexDirection: 'row', justifyContent: 'center' }}>
                    <Icon name="map" size={35} color={COLOR.grey400} />
                </View>
            )
        }
        return (
            <View style={{ marginLeft: 5, width: 70, flexDirection: 'row', justifyContent: 'flex-start' }}>
                {locationIds.slice(0, 3).map((locationId, i) =>
                    <Image
                        key={`route-location-icon-${i}`}
                        source={{ uri: `https://travellir.leers.pl/api/img/locations/${locationId}/icon` }}
                        style={{ width: 50, height: 50, position: 'absolute', left: (i * 10), top: -25 }}
                    />)}
            </View>
        );
    }
}

export default RouteIcon;