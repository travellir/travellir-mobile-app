import { Action } from 'redux';
import { connect } from 'react-redux';

export interface TravellirReducer<T> {
    (state: any, payload: T): any
}

export interface TravellirReducers {
    [key: string]: TravellirReducer<any>
}

export interface RawAction<T> extends Action<string> {
    type: string,
    payload: T
}

export interface TravellirModule<T> {
    reducers: TravellirReducers,
    initialState: T
}

export const connectState = <P> (component: React.ComponentClass<P>): React.ComponentClass<any> => {
    return connect((state: { travellir: any }) => state.travellir)(component);
}

export const connectRawState = <P> (component: React.ComponentClass<P>): React.ComponentClass<any> => {
    return connect(state => state)(component);
}