import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import { Divider } from 'react-native-material-ui';
import NavigatorService from '../navigation/NavigatorService';
import DrawerService from './DrawerService';

const styles = StyleSheet.create({
    requestLoginContainer: {
        paddingTop: 40,
        paddingHorizontal: 20,
        marginBottom: 20
    },
    requestLoginText: {
        color: '#dfdfdf',
        textAlign: 'center',
        fontSize: 16,
        marginBottom: 15,
        lineHeight: 30
    }
})

class RequestLoginAccountSegment extends Component {
    render() {
        return (
            <View>
                <View style={styles.requestLoginContainer}>
                    <Text style={styles.requestLoginText}>Zaloguj się, aby uzyskać dostęp do wszystkich funkcjonalności</Text>
                    <Button
                        onPress={this.navigateToLoginPage}
                        style={{
                            container: { backgroundColor: '#2A67C2' },
                            text: { color: '#dadada', fontSize: 16 }
                        }}
                        text='Logowanie' />
                </View>
                <Divider style={{ container: { backgroundColor: '#aaaaaa' } }} />
            </View>
        )
    }

    navigateToLoginPage =  async () => {
        DrawerService.toggleDrawer();
        NavigatorService.navigateTo('Login');
    }
}

export default RequestLoginAccountSegment