import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon, RippleFeedback } from 'react-native-material-ui';
import NavigatorService from '../navigation/NavigatorService';
import DrawerService from './DrawerService';
import { connectState } from '../travellir-redux';
import { LoginPageState } from '../login-page/login-page-redux';

interface Feature {
    icon: string,
    label: string,
    screen: string,
    loggedOnly?: boolean
}

const features: Array<Feature> = [
    {
        icon: 'map',
        label: 'Mapa główna',
        screen: 'TravellirMapScreen'
    },
    {
        icon: 'terrain',
        label: 'Moje trasy',
        screen: 'UserRoutesScreen'
    },
    {
        icon: 'favorite',
        label: 'Zapisane obiekty',
        screen: 'FavoriteLocationsScreen'
    },
    {
        loggedOnly: true,
        icon: 'photo-camera',
        label: 'Wysłane zdjęcia',
        screen: 'SentPhotosScreen'
    }
]

class FeaturesList extends React.PureComponent<LoginPageState> {
    render() {
        const { loggedUser } = this.props;
        return (
            <View style={styles.featuresContainer}>
                {features
                    .filter(feature => {
                        if (!feature.loggedOnly)
                            return true;
                        return !!loggedUser;
                    })
                    .map((feature, i) => <FeaturesListItem key={i} feature={feature} />)}
            </View>
        );
    }
}

interface ItemProps {
    feature: Feature
}

class FeaturesListItem extends Component<ItemProps> {

    render() {
        const { feature } = this.props;
        return (
            <RippleFeedback borderless={false} onPress={() => this.navigateToFeature(feature)} color='#606060'>
                <View style={styles.featureItemContainer}>
                    <Icon name={feature.icon} size={27} style={[styles.featureIcon]} color='#dadada' />
                    <Text style={styles.featureLabel}>{feature.label}</Text>
                </View>
            </RippleFeedback>)
    }

    navigateToFeature = (feature: Feature) => {
        if (feature.screen) {
            DrawerService.toggleDrawer();
            NavigatorService.navigateTo(feature.screen);
        }
    }

}

const styles = StyleSheet.create({
    featuresContainer: {
        marginTop: 20
    },
    featureItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 25
    },
    featureIcon: {
        marginRight: 20
    },
    featureLabel: {
        color: '#dadada',
        fontSize: 16
    }
})

export default connectState(FeaturesList);