import { RawAction, TravellirModule } from '../travellir-redux';

export interface DrawerState {
    drawer: {
        open: boolean
    }
}

export const types = {
    TOGGLE_DRAWER: 'TOGGLE_DRAWER'
}

export const actions = {
    toggleDrawer(): { type: string } {
        return {
            type: types.TOGGLE_DRAWER
        }
    }
}

export const DrawerModule = {
    initialState: {
        drawer: {
            open: false
        }
    },
    reducers: {
        [types.TOGGLE_DRAWER](state: DrawerState, payload: any) {
            return {
                drawer: {
                    open: !state.drawer.open
                }
            };
        }
    },
} as TravellirModule<DrawerState>;