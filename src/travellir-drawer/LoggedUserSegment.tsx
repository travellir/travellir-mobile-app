import * as React from 'react';
import { View, StyleSheet, Text, Image, Dimensions, } from 'react-native';
import { connectState } from '../travellir-redux';
import { LoginPageState } from '../login-page/login-page-redux';
import { Button } from 'react-native-material-ui';
import LoginScreenService from '../login-page/LoginScreenService';
import DrawerService from './DrawerService';
import NavigatorService from '../navigation/NavigatorService';
import * as md5 from 'react-native-md5';

const segmentBg = require('../../assets/drawer-bg.png');

const { width } = Dimensions.get('window');
const drawerWidth = width * .75;

export interface Props extends LoginPageState {
}

const styles = StyleSheet.create({
    bottomSheet: {
        padding: 10,
        backgroundColor: 'rgba(0, 0, 0, .5)',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 15
    }
})

class LoggedUserSegment extends React.PureComponent<Props, any> {
    render() {
        const { loggedUser } = this.props;
        if (loggedUser == null)
            return null;
        return (
            <View>
                <Image source={segmentBg} style={{ width: drawerWidth, height: 180 }} />
                <View style={{ position: 'absolute', bottom: 0, flexDirection: 'column', width: drawerWidth }}>
                    <View style={styles.iconContainer}>
                        <Image
                            style={{ width: 70, height: 70 }}
                            source={{ uri: `https://www.gravatar.com/avatar/${md5.hex_md5(loggedUser.email)}?d=identicon` }}
                            borderRadius={35} />
                    </View>
                    <View style={styles.bottomSheet}>
                        <Text style={{ fontSize: 14, color: '#fff' }}>@{loggedUser.email}</Text>
                        <Button
                            onPress={this.logOut}
                            text={'Wyloguj'}
                            style={{
                                text: { color: '#fff' },
                                container: { backgroundColor: 'rgba(255,255,255, .15)' }
                            }} />
                    </View>
                </View>
            </View>
        );
    }

    logOut = () => {
        DrawerService.toggleDrawer();
        LoginScreenService.logOut();
        NavigatorService.navigateTo('FavoriteLocationsScreen');
    }
}

export default connectState(LoggedUserSegment);