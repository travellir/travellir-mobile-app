import React, { Component } from 'react';
import { View, Dimensions, Text, StyleSheet, TouchableWithoutFeedback, Animated, Platform } from 'react-native';
import RequestLoginAccountSegment from './RequestLoginAccountSegment';
import { connectState } from '../travellir-redux';
import { DrawerState } from './travellir-drawer-redux';
import DrawerService from './DrawerService';
import FeaturesList from './FeaturesList';
import { LoginPageState } from '../login-page/login-page-redux';
import LoggedUserSegment from './LoggedUserSegment';


const { width, height } = Dimensions.get('window');
const drawerWidth = width * .75;

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        zIndex: 3,
        elevation: 6,
        top: 0,
        left: 0,
        height,
        width,
        backgroundColor: '#000',
        opacity: .3
    },
    drawer: {
        shadowColor: '#303030',
        shadowOffset: { width: 3, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        zIndex: 4,
        elevation: 7,
        position: 'absolute',
        backgroundColor: "#505050",
        top: 0,
        left: 0,
        height,
        width: drawerWidth
    },
    drawerView: Platform.select({
        ios: {
            zIndex: 10
        }
    })
})

interface Props extends DrawerState, LoginPageState {

}

class TravellirDrawer extends Component<Props, any> {

    drawerTranslate = new Animated.Value(-drawerWidth);
    overlayOpacity = new Animated.Value(0);

    render() {
        const { drawer, loggedUser } = this.props;
        return (
            <View style={styles.drawerView}>
                <Animated.View style={[styles.drawer, { transform: [{ translateX: this.drawerTranslate }] }]}>
                {
                    loggedUser ? <LoggedUserSegment /> : <RequestLoginAccountSegment />
                }
                    
                    <FeaturesList />
                </Animated.View>
                {drawer.open &&
                    <TouchableWithoutFeedback onPress={DrawerService.toggleDrawer}>
                        <Animated.View style={styles.overlay} />
                    </TouchableWithoutFeedback>
                }
            </View>
        );
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.drawer.open)
            this.openDrawer();
        else
            this.closeDrawer();
    }

    closeDrawer = () => {
        Animated.timing(this.drawerTranslate, {
            duration: 200,
            toValue: -drawerWidth,
            useNativeDriver: true
        }).start();

        Animated.timing(this.overlayOpacity, {
            duration: 70,
            toValue: 0,
            useNativeDriver: true
        }).start();
    }

    openDrawer = () => {
        Animated.timing(this.drawerTranslate, {
            duration: 200,
            toValue: 0,
            useNativeDriver: true
        }).start();

        Animated.timing(this.overlayOpacity, {
            duration: 70,
            toValue: .3,
            useNativeDriver: true
        }).start();
    }
}

export default connectState(TravellirDrawer);