import TravellirStore from '../TravellirStore';
import { actions } from './travellir-drawer-redux';

export default {
    toggleDrawer() {
        TravellirStore.dispatch(actions.toggleDrawer());
    }
}