import * as React from 'react';
import { View, StyleSheet, Text, Image, } from 'react-native';
import { SentPhotosState } from '../sent-photos-redux';
import { connectState } from '../../travellir-redux';
import ListLoaderIndicator from '../../material/ListLoaderIndicator';
import { IconToggle } from 'react-native-material-ui';
import NavigatorService from '../../navigation/NavigatorService';
import SentPhotosService from '../SentPhotosService';
import DeletePhotoDialog from './DeletePhotoDialog';

interface Props extends SentPhotosState {
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    darkBackground: {
        backgroundColor: '#000'
    },
    locationTitleContainer: {
        width: '100%',
        marginTop: 20,
        paddingLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    imageContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        maxHeight: '80%'
    },
    locationTitle: {
        color: '#fff',
        fontSize: 22
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

interface State {
    photoUrl: string | null,
    deleteDialogOpen: boolean
}

class PhotoScreen extends React.PureComponent<Props, State> {

    state = {
        photoUrl: null,
        deleteDialogOpen: false
    }

    render() {
        const { currentPhotoLocation, currentPhoto, photoLocations } = this.props;
        const { photoUrl } = this.state;
        if (!currentPhotoLocation || !currentPhoto) {
            return null;
        }
        const location = photoLocations[currentPhotoLocation.locationId];
        if (!location) {
            return null;
        }
        return (
            <View style={{ flex: 1 }}>
                <DeletePhotoDialog
                    location={location}
                    photo={currentPhoto}
                    open={this.state.deleteDialogOpen}
                    onRequestClose={() => this.setState({ deleteDialogOpen: false })} />
                <View style={[styles.screenContainer, styles.darkBackground]}>
                    <View style={styles.locationTitleContainer}>
                        <View style={{ width: '60%' }}>
                            <Text style={styles.locationTitle}>{location.name}</Text>
                        </View>
                        <View>
                            <IconToggle name={'clear'} color={'#fff'} size={30} onPress={NavigatorService.goBack} />
                        </View>
                    </View>
                    <View style={styles.imageContainer}>
                        {photoUrl ?
                            <Image 
                            loadingIndicatorSource={{uri: currentPhoto.thumbnailUrl }} 
                            source={{ uri: currentPhoto.thumbnailUrl }} resizeMode={'contain'} style={{ height: '95%', width: '100%' }} /> :
                            <ListLoaderIndicator />
                        }
                    </View>
                    <View style={styles.buttonContainer}>
                        <IconToggle onPress={() => this.setState({ deleteDialogOpen: true })} name={'delete'} color={'#fff'} size={30} />
                    </View>
                </View>
            </View>
        );
    }

    async componentDidMount() {
        const { currentPhoto } = this.props;
        if (currentPhoto) {
            const photoUrl = await SentPhotosService.photoUrl(currentPhoto.original);
            this.setState({ photoUrl })
        }
    }
}

export default connectState(PhotoScreen);