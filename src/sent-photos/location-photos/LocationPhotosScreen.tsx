import * as React from 'react';
import { View, StyleSheet, Text, Image, ActivityIndicator, } from 'react-native';
import ListScreen from '../../layout/ListScreen';
import TravellirDrawer from '../../travellir-drawer/TravellirDrawer';
import MenuButton from '../../menu/MenuButton';
import CardHeader from '../../material/CardHeader';
import { COLOR } from 'react-native-material-ui';
import ListLoaderIndicator from '../../material/ListLoaderIndicator';
import PhotosList from './PhotosList';
import { connectState } from '../../travellir-redux';
import { SentPhotosState } from '../sent-photos-redux';

interface Props extends SentPhotosState {
}

const header = require('../../../assets/big-photos-header.png');

class LocationPhotosScreen extends React.PureComponent<Props> {
    render() {
        const { currentPhotoLocation, photoLocations } = this.props;
        if (!currentPhotoLocation)
            return null;
        const location = photoLocations[currentPhotoLocation.locationId];
        return (
            <ListScreen outerElements={<TravellirDrawer />}>
                <View>
                    <MenuButton />
                    <Image defaultSource={header} source={{ uri: location.image }} style={{ height: 200 }} />
                    <CardHeader>
                        <Text style={{ fontSize: 18 }}>Wysłane zdjęcia <Text style={{ fontWeight: 'bold' }}>{location ? location.name : 'Ładowanie...'}</Text></Text>
                    </CardHeader>
                    <PhotosList photos={currentPhotoLocation.photos} />
                </View>
            </ListScreen>
        );
    }
}

export default connectState(LocationPhotosScreen);