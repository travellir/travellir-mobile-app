import * as React from 'react';
import { View, StyleSheet, Text, Image, } from 'react-native';
import { UserPhoto } from '../sent-photos-redux';
import GridList from '../../material/GridList';
import { RippleFeedback, COLOR } from 'react-native-material-ui';
import SentPhotosService from '../SentPhotosService';
import NavigatorService from '../../navigation/NavigatorService';

interface Props {
    photos: UserPhoto[]
}

class PhotosList extends React.PureComponent<Props, any> {
    render() {
        const { photos } = this.props;
        return (
            <GridList>
                {
                    photos.map((photo, i) => (
                        <RippleFeedback onPress={() => this.showPhoto(photo)} color={COLOR.blue500} key={`photo-list-item-${i}`}>
                            <View style={{ width: '48%', marginBottom: 10 }} >
                                <Image borderRadius={2} style={{ width: '100%', height: 200 }} source={{ uri: photo.thumbnailUrl }} />
                            </View>
                        </RippleFeedback>
                    ))
                }
                {photos.length % 2 == 1 && <View style={{ width: '47%' }}></View>}
            </GridList>
        );
    }

    showPhoto = async (photo: UserPhoto) => {
        SentPhotosService.setCurrentPhoto(photo)
        NavigatorService.navigateTo('PhotoScreen');
    }
}

export default PhotosList;