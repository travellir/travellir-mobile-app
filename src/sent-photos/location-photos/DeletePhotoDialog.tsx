import * as React from 'react';
import { View, StyleSheet, Text, Dimensions, } from 'react-native';
import { UserPhoto, SentPhotosState } from '../sent-photos-redux';
import { DialogProps } from '../../dialogs/DialogProperties';
import MaterialDialog from '../../dialogs/MaterialDialog';
import { Button, COLOR } from 'react-native-material-ui';
import { LocationListItem } from '../../travellir-map/MapTypes';
import SentPhotosService from '../SentPhotosService';
import { connectState } from '../../travellir-redux';

interface Props extends DialogProps, SentPhotosState {
    photo: UserPhoto
    location: LocationListItem
}

class DeletePhotoDialog extends React.PureComponent<Props, any> {
    render() {
        const { open, onRequestClose, location } = this.props;
        return (
            <MaterialDialog width={Dimensions.get('window').width * .9} open={open} onRequestClose={onRequestClose}>
                <View style={{ padding: 15 }}>
                    <Text style={{ fontSize: 18 }}>Na pewno chcesz skasować zdjęcie obiektu <Text style={{ fontWeight: 'bold' }}>{location.name}</Text>?</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                    <Button onPress={onRequestClose} style={{ text: { color: '#2A7EE0' } }} text={'Zamknij'} />
                    <Button onPress={this.deletePhoto} style={{ text: { color: COLOR.red400 } }} text={'Kasuj'} />
                </View>
            </MaterialDialog>
        );
    }

    deletePhoto = async () => {
        const { userPhotos, currentPhotoLocation, currentPhoto } = this.props;
        if (!currentPhotoLocation || !currentPhoto) {
            return;
        }

        SentPhotosService.deletePhoto(currentPhoto, currentPhotoLocation, userPhotos);
    }
}

export default connectState(DeletePhotoDialog);