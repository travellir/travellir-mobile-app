import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { PhotosListItem, LoadedPhotoLocations } from './sent-photos-redux';
import LocationPhotosListItem from './LocationPhotosListItem';
import GridList from '../material/GridList';
import SentPhotosService from './SentPhotosService';
import NavigatorService from '../navigation/NavigatorService';

interface Props {
    userPhotos: PhotosListItem[],
    photoLocations: LoadedPhotoLocations
}

class PhotoLocationsList extends React.PureComponent<Props, any> {
    render() {
        const { userPhotos, photoLocations } = this.props;
        return (
            <GridList>
                {
                    userPhotos.map((item: PhotosListItem) => (
                        <LocationPhotosListItem
                            onPress={() => this.setCurrentPhotoLocation(item)}
                            location={photoLocations[item.locationId]}
                            item={item}
                            key={`sent-photos-item-${item.locationId}`} />
                    ))
                }
                {userPhotos.length % 2 == 1 && <View style={{ width: '47%' }}></View>}
            </GridList>
        );
    }

    setCurrentPhotoLocation = async (location: PhotosListItem) => {
        SentPhotosService.setCurrentPhotoLocation(location);
        NavigatorService.navigateTo('LocationPhotosScreen');
    }
}
export default PhotoLocationsList;