import * as React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import SentPhotosService from './SentPhotosService';
import ListScreen from '../layout/ListScreen';
import CardHeader from '../material/CardHeader';
import MenuButton from '../menu/MenuButton';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import LocationPhotosListItem from './LocationPhotosListItem';
import { LocationListItem } from '../travellir-map/MapTypes';
import { PhotosListItem, SentPhotosState } from './sent-photos-redux';
import PhotoLocationsList from './PhotoLocationsList';
import { connectState } from '../travellir-redux';
import ListLoaderIndicator from '../material/ListLoaderIndicator';
import { LoginPageState } from '../login-page/login-page-redux';


interface Props extends SentPhotosState, LoginPageState {
}

const header = require('../../assets/big-photos-header.png');

class SentPhotosScreen extends React.PureComponent<Props> {

    render() {
        const { photoLocations, userPhotos, userPhotosLoading } = this.props;
        return (
            <ListScreen outerElements={<TravellirDrawer />}>
                <View>
                    <MenuButton />
                    <Image source={header} style={{ height: 200 }} />
                    <CardHeader text={'Wysłane zdjęcia'} />
                    {userPhotosLoading ?
                        <ListLoaderIndicator /> :
                        <PhotoLocationsList
                            userPhotos={userPhotos}
                            photoLocations={photoLocations}
                        />
                    }
                </View>
            </ListScreen>
        );
    }



    async componentDidMount() {
        const { loggedUser } = this.props;
        if (!loggedUser)
            return;
        const locationIds = await SentPhotosService.locationPhotos(loggedUser.uid);
        await SentPhotosService.fetchLocations(locationIds);
    }
}

export default connectState(SentPhotosScreen);