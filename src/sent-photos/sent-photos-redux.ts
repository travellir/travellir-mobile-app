import { TravellirReducers, RawAction, TravellirModule } from '../travellir-redux';
import { LocationListItem } from '../travellir-map/MapTypes';

export type LoadedPhotoLocations = { [key: number]: LocationListItem }

export interface UserPhoto {
    locationId: number,
    original: string,
    thumbnail: string,
    thumbnailUrl: string
}

export interface PhotosListItem {
    locationId: number,
    photos: UserPhoto[]
}

export interface SentPhotosState {
    userPhotos: PhotosListItem[],
    userPhotosLoading: boolean
    photoLocations: LoadedPhotoLocations,
    photoLocationsLoading: boolean
    currentPhotoLocation: PhotosListItem | null,
    currentPhoto: UserPhoto | null
}

export const types = {
    PHOTO_LOCATIONS_FETCH_START: 'PHOTO_LOCATIONS_FETCH_START',
    PHOTO_LOCATIONS_FETCHED: 'PHOTO_LOCATIONS_FETCHED',
    USER_PHOTOS_FETCHED: 'USER_PHOTOS_FETCHED',
    USER_PHOTOS_FETCH_START: 'USER_PHOTOS_FETCH_START',
    SET_CURRENT_PHOTO_LOCATION: 'SET_CURRENT_PHOTO_LOCATION',
    SET_CURRENT_PHOTO: 'SET_CURRENT_PHOTO',
    PHOTO_UPLOADED: 'PHOTO_UPLOADED',
}

export const actions = {
    photoUploaded(locationId: number, photo: UserPhoto): RawAction<{ locationId: number, photo: UserPhoto }> {
        return {
            type: types.PHOTO_UPLOADED,
            payload: {
                photo,
                locationId
            }
        }
    },
    locationsFetchStart(): RawAction<any> {
        return {
            type: types.PHOTO_LOCATIONS_FETCH_START,
            payload: true
        }
    },
    locationsFetched(locations: LoadedPhotoLocations): RawAction<LoadedPhotoLocations> {
        return {
            type: types.PHOTO_LOCATIONS_FETCHED,
            payload: locations
        }
    },
    userPhotosFetchStart(): RawAction<any> {
        return {
            type: types.USER_PHOTOS_FETCH_START,
            payload: true
        }
    },
    userPhotosFetched(photos: PhotosListItem[]): RawAction<PhotosListItem[]> {
        return {
            type: types.USER_PHOTOS_FETCHED,
            payload: photos
        }
    },
    setCurrentPhotoLocation(location: PhotosListItem): RawAction<PhotosListItem> {
        return {
            type: types.SET_CURRENT_PHOTO_LOCATION,
            payload: location
        }
    },
    setCurrentPhoto(photo: UserPhoto): RawAction<UserPhoto> {
        return {
            type: types.SET_CURRENT_PHOTO,
            payload: photo
        }
    }
}

export const SentPhotosModule = {
    initialState: {
        userPhotos: [],
        userPhotosLoading: false,
        photoLocations: {},
        photoLocationsLoading: false,
        currentPhotoLocation: null,
        currentPhoto: null
    },
    reducers: {
        [types.PHOTO_LOCATIONS_FETCH_START](state: SentPhotosState) {
            return {
                photoLocationsLoading: true
            }
        },
        [types.PHOTO_LOCATIONS_FETCHED](state: SentPhotosState, locations: LoadedPhotoLocations) {
            return {
                photoLocationsLoading: false,
                photoLocations: locations
            }
        },
        [types.USER_PHOTOS_FETCH_START](state: SentPhotosState) {
            return {
                userPhotosLoading: true
            }
        },
        [types.USER_PHOTOS_FETCHED](state: SentPhotosState, userPhotos: PhotosListItem[]) {
            return {
                userPhotosLoading: false,
                userPhotos
            }
        },
        [types.SET_CURRENT_PHOTO](state: SentPhotosState, photo: UserPhoto) {
            return {
                currentPhoto: photo
            }
        },
        [types.SET_CURRENT_PHOTO_LOCATION](state: SentPhotosState, location: PhotosListItem) {
            return {
                currentPhotoLocation: location
            }
        },
        [types.PHOTO_UPLOADED](state: SentPhotosState, payload: { locationId: number, photo: UserPhoto }) {
            const { locationId, photo } = payload;
            const photosListItem = state.userPhotos[locationId];
            if (photosListItem) {
                const updatedItem = Object.assign({}, photosListItem, {
                    photos: [photo].concat(photosListItem.photos)
                })
                return {
                    userPhotos: Object.assign({}, state.userPhotos, {
                        [locationId]: updatedItem
                    })
                }
            }
            return {
                userPhotos: Object.assign({}, state.userPhotos, {
                    [locationId]: {
                        locationId,
                        photos: [photo]
                    }
                })
            }
        }
    },
} as TravellirModule<SentPhotosState>;