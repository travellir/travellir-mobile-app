import * as React from 'react';
import { View, StyleSheet, Text, Image, ActivityIndicator, } from 'react-native';
import SentPhotosService from './SentPhotosService';
import { COLOR } from 'react-native-material-ui';
import { LocationListItem } from '../travellir-map/MapTypes';
import { RippleFeedback } from 'react-native-material-ui';
import { PhotosListItem } from './sent-photos-redux';

interface Props {
    item: PhotosListItem
    location?: LocationListItem,
    onPress?: () => void
}

interface State {
    photoUrl: string | null
}

class LocationPhotosListItem extends React.PureComponent<Props> {

    render() {
        const { item, location, onPress } = this.props;
        return (
            <RippleFeedback onPress={onPress} color={COLOR.blue300}>
                <View style={{ width: '47%', marginBottom: 10 }}>
                    <View style={{ height: 100, backgroundColor: COLOR.grey300, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image borderRadius={2} source={{ uri: item.photos[0].thumbnailUrl }} style={{ height: 100, width: '100%' }} />
                    </View>
                    {location ? <Text>{location.name}</Text> : <Text>Ładowanie...</Text>}
                </View>
            </RippleFeedback>
        );
    }
}

export default LocationPhotosListItem;