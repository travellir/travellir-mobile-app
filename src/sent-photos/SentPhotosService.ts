import firebase from 'firebase';
import Requests from '../http/Requests';
import { LocationListItem } from '../travellir-map/MapTypes';
import { UserPhoto, PhotosListItem, actions, LoadedPhotoLocations } from './sent-photos-redux';
import TravellirStore from '../TravellirStore';
import NavigatorService from '../navigation/NavigatorService';

async function deletePhoto(photo: UserPhoto) {
    const snapshot = (await firebase.database()
        .ref()
        .child('sent-photos')
        .orderByChild("original")
        .equalTo(photo.original)
        .once('value')) as firebase.database.DataSnapshot;

    const value = snapshot.val();

    if (value !== null) {
        const nodeIds = Object.keys(value)
        for (const nodeId of nodeIds) {
            firebase.database()
                .ref()
                .child('sent-photos')
                .child(nodeId)
                .remove();
        }

        firebase.storage().ref(photo.original).delete();
        firebase.storage().ref(photo.thumbnail).delete();
    }

}

export default {
    async findUserPhotos(userId: string): Promise<UserPhoto[]> {
        const result = await firebase.database()
            .ref()
            .child('sent-photos')
            .orderByChild("userUid")
            .equalTo(userId)
            .once('value') as firebase.database.DataSnapshot;


        const resultVal = result.val();
        if (resultVal === null)
            return [];
        const data: UserPhoto[] = [];

        Object.keys(resultVal).forEach((key) => {
            const record = resultVal[key];
            data.push({
                original: record.original,
                thumbnail: record.thumbnail,
                thumbnailUrl: record.thumbnailUrl,
                locationId: record.locationId
            })
            return true;
        })

        return data;
    },
    async locationPhotos(userId: string): Promise<number[]> {
        TravellirStore.dispatch(actions.userPhotosFetchStart())
        const photos = await this.findUserPhotos(userId);
        if (photos.length === 0) {
            TravellirStore.dispatch(actions.userPhotosFetched([]));
            return [];
        }
        const index: { [key: number]: UserPhoto[] } = {};
        for (const photo of photos) {
            if (index[photo.locationId]) {
                index[photo.locationId].push(photo)
            }
            else {
                index[photo.locationId] = [photo];
            }
        }

        const result: PhotosListItem[] = Object.keys(index).map(key => {
            const locationId = parseInt(key);
            const photos = index[locationId];

            return {
                locationId,
                photos
            }
        });

        TravellirStore.dispatch(actions.userPhotosFetched(result));
        return result.map(item => item.locationId);
    },

    photoUrl(path: string): Promise<string> {
        return firebase.storage().ref(path).getDownloadURL();
    },
    async fetchLocations(locationIds: number[]) {
        const parameters = locationIds
            .map(locationId => `locationIds=${locationId}`)
            .join('&')

        const locations = (await Requests.jsonGet('/api/locations?' + parameters)) as Array<LocationListItem>;

        const loadedLocations: LoadedPhotoLocations = {};
        for (const location of locations) {
            loadedLocations[location.id] = location;
        }
        TravellirStore.dispatch(actions.locationsFetched(loadedLocations))
    },

    setCurrentPhotoLocation(location: PhotosListItem) {
        TravellirStore.dispatch(actions.setCurrentPhotoLocation(location));
    },
    setCurrentPhoto(photo: UserPhoto) {
        TravellirStore.dispatch(actions.setCurrentPhoto(photo));
    },
    async deletePhoto(photo: UserPhoto, location: PhotosListItem, photoLocations: PhotosListItem[]) {
        if (location.photos.length === 1) {
            const updatedPhotoLocations = photoLocations
                .filter(_photoLocation => _photoLocation.locationId !== location.locationId);


            TravellirStore.dispatch(actions.userPhotosFetched(updatedPhotoLocations));
            await deletePhoto(photo);
            NavigatorService.navigateTo('SentPhotosScreen');
        }
        else {
            const updatedPhotos = location.photos.filter(_photo => _photo.original !== photo.original)
            const updatedListItem = Object.assign({}, location, {
                photos: updatedPhotos
            })

            const updatedPhotoLocations = photoLocations.map(_photoLocation => {
                if (_photoLocation.locationId === updatedListItem.locationId)
                    return updatedListItem;
                return _photoLocation;
            });



            TravellirStore.dispatch(actions.setCurrentPhotoLocation(updatedListItem))
            TravellirStore.dispatch(actions.userPhotosFetched(updatedPhotoLocations));
            deletePhoto(photo);
            NavigatorService.goBack()
        }

    }
}