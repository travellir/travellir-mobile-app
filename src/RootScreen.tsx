import React, { Component } from 'react';
import { View } from 'react-native';
import TravellirNavigation from './navigation/TravellirNavigator';
import TravellirDrawer from './travellir-drawer/TravellirDrawer';

class RootScreen extends Component {
    render() {
        return (
            <TravellirNavigation />
        );
    }
}

export default RootScreen;