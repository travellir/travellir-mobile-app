import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import ContainerView from '../layout/ContainerView';
import SearchPanel from './SearchPanel';
import { SearchHit } from './locations-search-types';
import SearchService from './SearchService';
import SearchHitsList from './SearchHitsList';
import SavedSearchesList from './SavedSearchesList';
import LoadingPage from '../material/LoadingPage';

interface Props {
}
interface State {
    hits: SearchHit[],
    hitsLoading: boolean,
    phrase: string
}



class LocationSearchScreen extends React.PureComponent<Props, State> {

    state = {
        hits: [],
        hitsLoading: false,
        phrase: ''
    }

    render() {
        const { hits, hitsLoading, phrase } = this.state;
        return (
            <ContainerView>
                <SearchPanel
                    phrase={phrase}
                    onPhraseChanged={phrase => this.setState({ phrase })}
                    loading={hitsLoading}
                    onSearch={this.performSearch} />
                {hitsLoading &&
                    <LoadingPage />
                }
                {phrase.trim().length === 0 && !hitsLoading &&
                    <SavedSearchesList
                        onSearchSelected={(phrase: string) => this.performSavedSearch(phrase)}
                    />}
                {hits.length > 0 && <SearchHitsList hits={hits} />}
            </ContainerView>
        );
    }

    performSavedSearch = async (phrase: string) => {
        this.setState({ phrase, hitsLoading: true })
        const hits = await SearchService.search(phrase);
        this.setState({
            hits,
            hitsLoading: false
        })
    }
    performSearch = async (phrase: string) => {
        if (phrase.trim().length === 0) {
            this.setState({
                hits: [],
                hitsLoading: false
            });
            return;
        }
        this.setState({ hitsLoading: true })
        const hits = await SearchService.search(phrase);
        this.setState({
            hits,
            hitsLoading: false
        })
    }
}

export default LocationSearchScreen;