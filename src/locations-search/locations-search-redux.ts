import { RawAction, TravellirModule } from '../travellir-redux';
import { SavedSearches } from './locations-search-types';

export interface LocationsSearchState {
    savedSearches: SavedSearches
}


export const types = {
    SAVED_SEARCHES_FETCHED: 'SAVED_SEARCHES_FETCHED'
}

export const actions = {
    savedSearchesFetched(savedSearches: SavedSearches): RawAction<SavedSearches> {
        return {
            type: types.SAVED_SEARCHES_FETCHED,
            payload: savedSearches
        }
    }
}

export const LocationsSearchModule = {
    initialState: {
        savedSearches: []
    },
    reducers: {
        [types.SAVED_SEARCHES_FETCHED](state: LocationsSearchState, savedSearches: SavedSearches) {
            return {
                savedSearches: savedSearches
            }
        }
    }
} as TravellirModule<LocationsSearchState>