import React, { Component } from 'react';
import { Icon, RippleFeedback, COLOR } from 'react-native-material-ui';
import { Text, Dimensions, View, StyleSheet, TextInput } from 'react-native';
import DrawerService from '../travellir-drawer/DrawerService';
import NavigatorService from '../navigation/NavigatorService';
import { IconToggle } from 'react-native-material-ui';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    statusBar: {
        marginTop: 25,
        zIndex: 1,
        width: width,
    },
    card: {
        backgroundColor: '#fff',
        shadowColor: '#303030',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 6
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

interface Props {
    loading: boolean,
    phrase: string
    onPhraseChanged: (phrase: string) => void
    onSearch: (phrase: string) => void
}

class SearchPanel extends Component<Props> {

    private debounce: NodeJS.Timer | null = null;

    render() {
        const { phrase, onPhraseChanged } = this.props;
        return (
            <View style={[styles.statusBar, styles.card, styles.row]}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 5 }}>
                    {this.menuIcon()}
                    <TextInput
                        autoFocus={true}
                        style={{ width: width * .72, fontSize: 16 }}
                        placeholder='Wyszukaj obiekty'
                        onChangeText={this.onPhraseChange}
                        value={phrase}
                        underlineColorAndroid="#fff" />
                    {phrase.trim().length > 0 && <IconToggle onPress={this.clear} name="clear" size={20} color={COLOR.grey700} />}
                </View>
            </View>
        );
    }

    clear = () => {
        this.props.onPhraseChanged('');
        this.props.onSearch('');
    }

    onPhraseChange = (phrase: string) => {
        this.props.onPhraseChanged(phrase)
        if (this.debounce) {
            clearTimeout(this.debounce);
        }
        this.debounce = setTimeout(() => this.props.onSearch(phrase), 1000)
    }

    private menuIcon() {
        return (<View style={{ borderRadius: 25, marginLeft: -10, marginRight: 10 }}>
            <IconToggle name="arrow-back" color={COLOR.grey600} onPress={NavigatorService.goBack} />
        </View>);
    }
}

export default SearchPanel;