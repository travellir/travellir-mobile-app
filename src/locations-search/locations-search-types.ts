import { LocationListItem } from '../travellir-map/MapTypes';

export type SavedSearches = string[]

export interface SearchHit extends LocationListItem {
}
