import { SearchHit, SavedSearches } from './locations-search-types';
import Requests from '../http/Requests';
import { AsyncStorage } from 'react-native';
import TravellirStore from '../TravellirStore';
import { actions } from './locations-search-redux';

const savedSearchesKey = 'travellir/saved-searches'

async function readSavedSearches(): Promise<SavedSearches> {
    const json = await AsyncStorage.getItem(savedSearchesKey);
    const storedItems = JSON.parse(json)
        .filter((phrase: string) => (!!phrase));

    const distinct: SavedSearches = [];

    for (const item of storedItems) {
        if (!distinct.includes(item)) {
            distinct.push(item)
        }
    }

    return distinct;
}

async function fetchSavedSearches() {
    const savedSearches = await readSavedSearches();
    TravellirStore.dispatch(actions.savedSearchesFetched(savedSearches));
}

fetchSavedSearches();

export default {
    fetchSavedSearches,
    saveSearch(phrase: string, savedSearches: SavedSearches) {

        const _searches = savedSearches.includes(phrase) ? savedSearches.filter(_phrase => _phrase !== phrase) : savedSearches;
    
        const updatedSearches = [phrase].concat(_searches).slice(0, 10);
        AsyncStorage.setItem(savedSearchesKey, JSON.stringify(updatedSearches));
        TravellirStore.dispatch(actions.savedSearchesFetched(updatedSearches));
    },
    async search(phrase: string): Promise<SearchHit[]> {
        const uri = `/api/public/search/locations?q=${encodeURIComponent(phrase.toLowerCase())}`;
        return Requests.jsonGet(uri);
    }
}