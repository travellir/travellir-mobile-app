import * as React from 'react';
import { View, StyleSheet, Text, FlatList, } from 'react-native';
import { SearchHit } from './locations-search-types';
import LocationListCard from '../material/LocationListCard';
import { Icon } from 'react-native-material-ui';
import { COLOR } from 'react-native-material-ui';
import MarkersService from '../travellir-map/MarkersService';
import CalloutService from '../travellir-map/callouts/CalloutService';
import NavigatorService from '../navigation/NavigatorService';
import SearchService from './SearchService';
import { connectState } from '../travellir-redux';
import { LocationsSearchState } from './locations-search-redux';
import DistanceInfo from '../localization/DistanceInfo';

interface Props extends LocationsSearchState {
    hits: SearchHit[]
}

class SearchHitsList extends React.PureComponent<Props, any> {
    render() {
        const { hits } = this.props;
        return (
            <FlatList
                contentContainerStyle={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 10
                }}
                data={hits}
                keyExtractor={(item, index) => `search-hit-${item.id}`}
                renderItem={({ item, index }: { item: SearchHit, index: number }) =>
                    <LocationListCard
                        onPress={() => this.showLocation(item)}
                        rightElement={
                            <View>
                                <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingRight: 10 }}>
                                    <Icon name="map" color={COLOR.deepOrange600} size={25} />
                                    <DistanceInfo coords={item.coords} />
                                </View>
                            </View>
                        }
                        key={`search-hit-${item.id}`}
                        location={item} />}
            />
        );
    }

    showLocation = (hit: SearchHit) => {
        SearchService.saveSearch(hit.name.toLowerCase(), this.props.savedSearches);
        setTimeout(() => {
            MarkersService.panToLocation(hit);
            CalloutService.toggleCallout(hit.id);
            NavigatorService.navigateTo('TravellirMapScreen');
        }, 100)
    }
}

export default connectState(SearchHitsList);