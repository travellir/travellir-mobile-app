import * as React from 'react';
import { View, StyleSheet, Text, FlatList, } from 'react-native';
import { connectState } from '../travellir-redux';
import { LocationsSearchState } from './locations-search-redux';
import { Icon, COLOR } from 'react-native-material-ui';
import { RippleFeedback } from 'react-native-material-ui';

export interface Props extends LocationsSearchState {
    onSearchSelected: (phrase: string) => void
}

class SavedSearchesList extends React.PureComponent<Props> {
    render() {
        const { savedSearches, onSearchSelected } = this.props;
        return (
            <FlatList
                contentContainerStyle={{
                    paddingVertical: 10
                }}
                data={savedSearches}
                keyExtractor={(item, index) => `search-hit-${index}`}
                renderItem={({ item, index }: { item: string, index: number }) =>
                    <SavedSearchListItem onPress={() => onSearchSelected(item)} key={`search-hit-${index}`} phrase={item} />}
            />
        );
    }
}

interface SavedSearchListItemProps {
    phrase: string,
    onPress: () => void
}
class SavedSearchListItem extends React.PureComponent<SavedSearchListItemProps> {
    render() {
        const { phrase, onPress } = this.props;
        return (
            <RippleFeedback onPress={onPress} color={COLOR.blue400} borderless={false}>
                <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{paddingRight: 10}}>
                            <Icon name={'access-time'} color={COLOR.grey600} />
                        </View>
                        <Text>{phrase}</Text>
                    </View>
                    <View>
                        <Icon name={'call-made'} color={COLOR.grey600} />
                    </View>
                </View>
            </RippleFeedback>
        )
    }
}

export default connectState(SavedSearchesList);