import { RawAction, TravellirModule } from '../travellir-redux';
import { Coords } from '../travellir-map/MapTypes';

export interface LocalizationState {
    permissionGranted: boolean,
    currentLocation: Coords | null
}

export const types = {
    LOCALIZATION_PERMISSION_GRANTED: 'LOCALIZATION_PERMISSION_GRANTED',
    CURRENT_POSITION_FETCHED: 'CURRENT_POSITION_FETCHED'
}

export const actions = {
    permissionGranted(): RawAction<any> {
        return {
            type: types.LOCALIZATION_PERMISSION_GRANTED,
            payload: null
        }
    },
    currentPositionFetched(coords: Coords): RawAction<Coords> {
        return {
            type: types.CURRENT_POSITION_FETCHED,
            payload: coords
        }
    }
}

export const LocalizationModule = {
    initialState: {
        permissionGranted: false,
        currentLocation: null
    },
    reducers: {
        [types.LOCALIZATION_PERMISSION_GRANTED](state: LocalizationState) {
            return {
                permissionGranted: true
            };
        },
        [types.CURRENT_POSITION_FETCHED](state: LocalizationState, payload: Coords) {
            return {
                currentLocation: payload
            };
        }
    },
} as TravellirModule<LocalizationState>;