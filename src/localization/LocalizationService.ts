import { Permissions, Location } from 'expo';
import TravellirStore from '../TravellirStore';
import { actions } from './localization-redux';
import { Coords } from '../travellir-map/MapTypes';
import { ToastAndroid } from 'react-native';

export default {
    async askPersmission() {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status === 'granted') {
            TravellirStore.dispatch(actions.permissionGranted());
        }


    },
    async fetchCurrentPosition() {
        try {

            const location = await Location.getCurrentPositionAsync({});
            const coords: Coords = {
                lat: location.coords.latitude,
                lng: location.coords.longitude
            };
            TravellirStore.dispatch(actions.currentPositionFetched(coords))
        }
        catch (err) {
            ToastAndroid.show(
                'Nie udało się pobrać Twojej lokalizacji',
                2000
            );
        }
    }
}