import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { connectState } from '../travellir-redux';
import { LocalizationState } from './localization-redux';
import { COLOR } from 'react-native-material-ui';
import { Coords } from '../travellir-map/MapTypes';

interface Props {
    coords: Coords
}
const ONE_DEGREE_IN_METERS = 111_196.672;

class DistanceInfo extends React.PureComponent<Props & LocalizationState, any> {
    render() {
        const { currentLocation, coords } = this.props;
        if (!currentLocation) {
            return null;
        }
        return (
            <Text style={{ fontSize: 12, color: COLOR.grey500 }}>{this.distanceBetween(currentLocation, coords)}</Text>
        );
    }

    distanceBetween = (coords1: Coords, coords2: Coords): string => {
        const distance = Math.sqrt(Math.pow((coords1.lat - coords2.lat), 2) + Math.pow((coords1.lng - coords2.lng), 2));
        const distanceInMeters = distance * ONE_DEGREE_IN_METERS;

        const kilometers = Math.trunc(distanceInMeters / 1000)
        const meters = Math.trunc(distanceInMeters % 1000);

        if (kilometers > 100)
            return `~${kilometers}km`;

        return `${kilometers}km ${meters}m`
    }

}

export default connectState(DistanceInfo);