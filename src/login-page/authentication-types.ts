export interface FirebaseUser {
    "uid": string,
    "displayName": string,
    "email": string
}

export interface FirebaseAuthResponse {
    user: FirebaseUser
}

export interface TravellirUser {
    "uid": string,
    "displayName": string,
    "email": string
}