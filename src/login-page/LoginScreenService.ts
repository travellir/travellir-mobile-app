import TravellirStore from '../TravellirStore';
import { actions } from './login-page-redux';
import firebase from 'firebase';
import { FirebaseAuthResponse, TravellirUser } from './authentication-types';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyCZp3EF9P10eaYb4UqRTaAZC_LKxiHluo4",
    authDomain: "travellir-mobile-1521915412568.firebaseapp.com",
    databaseURL: "https://travellir-mobile-1521915412568.firebaseio.com",
    projectId: "travellir-mobile-1521915412568",
    storageBucket: "travellir-mobile-1521915412568.appspot.com",
    messagingSenderId: "37043676"
};
if (firebase.apps.length === 0) {
    console.debug('Firebase initialized.')
    firebase.initializeApp(firebaseConfig);
}

(async () => {
    const user = await firebase.auth().currentUser;
    if (!user)
        return;
    const travellirUser: TravellirUser = {
        uid: user.uid,
        email: user.email || '',
        displayName: user.displayName || ''
    };
    TravellirStore.dispatch(actions.userLoggedIn(travellirUser));
})();

export default {
    async authenticate(email: string, password: string) {
        try {
            const result = await firebase.auth().signInWithEmailAndPassword(email, password) as FirebaseAuthResponse;
            const { user } = result;
            const travellirUser: TravellirUser = {
                uid: user.uid,
                email: user.email,
                displayName: user.displayName,
            };
            TravellirStore.dispatch(actions.userLoggedIn(travellirUser));
        }
        catch (err) {
            throw err;
        }
    },

    async logOut() {
        await firebase.auth().signOut();
        TravellirStore.dispatch(actions.userLoggedOut())
    }
}
