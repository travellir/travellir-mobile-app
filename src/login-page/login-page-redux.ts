import { TravellirReducers, RawAction, TravellirModule } from '../travellir-redux';
import { TravellirUser } from './authentication-types';

export interface LoginPageState {
    loggedUser: TravellirUser | null
}

export const types = {
    USER_LOGGED_IN: 'USER_LOGGED_IN',
    USER_LOGGED_OUT: 'USER_LOGGED_OUT'
}

export const actions = {
    userLoggedIn(user: TravellirUser): RawAction<TravellirUser> {
        return {
            type: types.USER_LOGGED_IN,
            payload: user
        }
    },
    userLoggedOut(): RawAction<any> {
        return {
            type: types.USER_LOGGED_OUT,
            payload: null
        }
    }
}

export const LoginPageModule = {
    initialState: {
        loggedUser: null
    },
    reducers: {
        [types.USER_LOGGED_IN](state: LoginPageState, user: TravellirUser) {
            return { 
                loggedUser: user 
            };
        },
        [types.USER_LOGGED_OUT](state: LoginPageState) {
            return {
                loggedUser: null
            }
        }
    },
} as TravellirModule<LoginPageState>;