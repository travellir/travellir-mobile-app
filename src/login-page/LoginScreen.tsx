import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    KeyboardAvoidingView,
    TouchableHighlight,
    Keyboard,
    Animated,
    ActivityIndicator
} from "react-native";
import { Font } from 'expo';
import { NavigationInjectedProps } from 'react-navigation';
import LoginButton from './LoginButton';
import LoginScreenService from './LoginScreenService';
import { connectState } from '../travellir-redux';
import { LoginPageState } from './login-page-redux';
import NavigatorService from '../navigation/NavigatorService';
import { COLOR } from 'react-native-material-ui';

const TravellirLogo = require('../../assets/travellir-logo.png');
const UbuntuFont = require('../../assets/fonts/Ubuntu-Bold.ttf');

type State = {
    travellirFont?: string,
    username: string,
    password: string
    loading: boolean,
    error: boolean
}

interface Props extends LoginPageState {
}

class LoginScreen extends Component<Props, State> {
    state = {
        travellirFont: undefined,
        username: "",
        password: "",
        loading: false,
        error: false

    };

    inputsPadding = new Animated.Value(30);
    logoMargin = new Animated.Value(120);

    render() {
        const { travellirFont, error, loading } = this.state;
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.logo, { marginTop: this.logoMargin }]}>
                    <Animated.Image source={TravellirLogo} style={{ width: 90, height: 90 }} />
                    <Text
                        style={{
                            color: "#f0f0f0",
                            marginTop: 5,
                            fontSize: 35,
                            textAlign: 'center',
                            fontFamily: travellirFont
                        }}>Travellir</Text>

                    <View style={{ marginTop: 10 }}>
                        {error && <Text style={{ color: COLOR.red900, fontSize: 16 }}>Podane poświadczenia nie są poprawne</Text>}
                        {loading && <ActivityIndicator color={COLOR.red400} size={30} />}
                    </View>
                </Animated.View>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    <Animated.View style={[styles.inputs, { paddingBottom: this.inputsPadding }]}>
                        <TextInput
                            placeholder={'E-mail'}
                            underlineColorAndroid="#fff"
                            style={[styles.textInput, styles.loginInput]}
                            onChangeText={username => this.setState({ username })}
                            value={this.state.username} />
                        <TextInput
                            placeholder={'Hasło'}
                            underlineColorAndroid="#fff"
                            secureTextEntry={true}
                            style={styles.textInput}
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password} />
                        <LoginButton disabled={loading} onLogin={this.onLogin} />
                    </Animated.View>
                </KeyboardAvoidingView>
            </View>
        );
    }

    async componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
        Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);

        await Font.loadAsync({
            'Ubuntu': UbuntuFont,
        });
        this.setState({ travellirFont: 'Ubuntu' })
    }

    keyboardWillShow = (event: any) => {
        Animated.parallel([
            Animated.timing(this.inputsPadding, {
                duration: 100,
                toValue: 10,
            }),
            Animated.timing(this.logoMargin, {
                duration: 100,
                toValue: 10,
            }),
        ]).start();
    };


    keyboardWillHide = (event: any) => {
        Animated.parallel([
            Animated.timing(this.inputsPadding, {
                duration: 100,
                toValue: 30,
            }),
            Animated.timing(this.logoMargin, {
                duration: 100,
                toValue: 120,
            }),
        ]).start();
    };


    onLogin = async () => {
        Keyboard.dismiss();
        const { username, password } = this.state;
        try {
            this.setState({ loading: true, error: false })
            await LoginScreenService.authenticate(username, password);
            NavigatorService.goBack();
            this.setState({ loading: false, error: false })
        }
        catch (err) {
            this.setState({ loading: false, error: true })
        }
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "#82b1ff",
    },
    logo: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        paddingTop: 20
    },
    inputs: {
        flex: 1,
        flexDirection: 'column',
        width: "100%",
        justifyContent: 'flex-end',
        alignItems: "center",

    },
    textInput: {
        height: 40,
        padding: 10,
        backgroundColor: "#fff",
        borderRadius: 2,
        width: "80%"
    },
    loginInput: {
        marginBottom: 10
    }
});

export default connectState(LoginScreen);
