import React, { Component } from 'react';
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import { Button, COLOR } from 'react-native-material-ui';

interface Props {
    onLogin: () => void,
    disabled: boolean
}

class LoginButton extends Component<Props> {
    render() {
        const { onLogin, disabled } = this.props;
        return (
            <TouchableHighlight
                style={{ width: "80%", marginTop: 10 }}
                onPress={onLogin}>
                <Button text={'ZALOGUJ SIĘ'}
                    disabled={disabled}
                    onPress={onLogin}
                    raised={true}
                    style={{
                        container: { backgroundColor: COLOR.blue700 },
                        text: { color: '#f0f0f0' }
                    }} />

            </TouchableHighlight>
        );
    }
}

export default LoginButton;