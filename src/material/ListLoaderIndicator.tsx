import * as React from 'react';
import { View, StyleSheet, Text, ActivityIndicator, } from 'react-native';
import { COLOR } from 'react-native-material-ui';

interface Props {
}

class ListLoaderIndicator extends React.PureComponent<Props, any> {
    render() {
        return (
            <View style={{ height: 150, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator animating={true} color={COLOR.blue500} size={80} />
            </View>
        );
    }
}

export default ListLoaderIndicator;