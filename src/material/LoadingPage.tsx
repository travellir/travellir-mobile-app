import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { COLOR } from 'react-native-material-ui';

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

export default class LoadingPage extends React.PureComponent<any, any> {
    render() {
        return (
            <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
                <ActivityIndicator size={80} color={COLOR.blue500} />
            </View>
        )
    }
} 