import React, { Component, ReactNode } from 'react';
import { Card } from 'react-native-material-ui';
import { Text, StyleSheet } from 'react-native';

interface Props {
    text?: string,
    children?: JSX.Element
}

const styles = StyleSheet.create({
    card: {
        padding: 13,
        marginTop: -33
    }
})

class CardHeader extends Component<Props> {
    render() {
        const { text, children } = this.props;
        return (
            <Card style={{ container: styles.card }}>
                {
                    text ? <Text style={{ fontSize: 19 }}>{text}</Text> : children
                }
            </Card>
        )
    }
};

export default CardHeader;