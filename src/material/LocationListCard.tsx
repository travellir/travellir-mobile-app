import * as React from 'react';
import { View, StyleSheet, Text, Image, Dimensions, } from 'react-native';
import { LocationListItem } from '../travellir-map/MapTypes';
import CardListItem from './CardListItem';
import { RippleFeedback } from 'react-native-material-ui';
import { COLOR } from 'react-native-material-ui';

interface Props {
    location: LocationListItem,
    rightElement?: React.ReactNode
    onPress?: () => void
}

const { width } = Dimensions.get('window');

class LocationListCard extends React.PureComponent<Props, any> {
    render() {
        const { location, rightElement, onPress } = this.props;
        return (
            <CardListItem onPress={onPress} accessible={true} rippleColor={COLOR.orange300} style={{ justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ padding: 5 }}>
                        <Image source={{ uri: location.icon }} style={{ width: 50, height: 50 }} />
                    </View>
                    <View style={{ flexDirection: 'column', paddingVertical: 15, width: width * .95 * .62 }}>
                        <Text style={{ fontSize: 19, marginBottom: 2 }} numberOfLines={1}>{location.name}</Text>
                        <Text style={{ fontSize: 14, color: COLOR.grey700 }} numberOfLines={1}>{location.address}</Text>
                    </View>
                </View>
                {rightElement}
            </CardListItem>
        );
    }
}

export default LocationListCard;