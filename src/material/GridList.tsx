import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';

interface Props {
}

const styles = StyleSheet.create({
    gridList: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    }
})

class GridList extends React.PureComponent<Props, any> {
    render() {
        return (
            <View style={styles.gridList}>
                {this.props.children}
            </View>
        );
    }
}

export default GridList;