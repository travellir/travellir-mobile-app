import React, { Component } from 'react';
import { StyleSheet, Image, View, Dimensions, ViewStyle } from 'react-native';
import { RippleFeedback } from 'react-native-material-ui';

const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    card: {
        backgroundColor: '#fff',
        borderRadius: 2,
        elevation: 2,
        marginVertical: 3,
        width: width * .95
    },
    contentContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
});

interface Props {
    accessible?: boolean,
    onPress?: () => void,
    rippleColor?: string
    style?: ViewStyle
}
class CardListItem extends Component<Props> {
    render() {
        return (
            <View style={styles.card}>
                {this.renderContent(<View style={[styles.contentContainer, this.props.style]}>
                    {this.props.children}
                </View>)}
            </View>
        )
    }

    private renderContent(children: JSX.Element): React.ReactNode {
        const { accessible, onPress, rippleColor } = this.props;
        if (!accessible)
            return children;
        return (<RippleFeedback color={rippleColor} onPress={onPress}>
            {children}
        </RippleFeedback>)
    }
};

export default CardListItem;