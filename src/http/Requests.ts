
const apiOrigin = 'https://travellir.leers.pl'

class Requests {

    static async jsonGet(path: string) {
        const response = await fetch(Requests._linkTo(path), {
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'GET'
        });

        Requests._checkRequestIsAuthorized(response);
        return response.json();
    }

    static async jsonPost(path: string, data: any) {
        const response = await fetch(Requests._linkTo(path), {
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });

        Requests._checkRequestIsAuthorized(response);
        return response;
    }

    static async jsonPut(path: string, data: any) {
        const response = await fetch(Requests._linkTo(path), {
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'PUT'
        });

        Requests._checkRequestIsAuthorized(response);
        return response;
    }

    static async jsonDelete(path: string) {
        const response = await fetch(Requests._linkTo(path), {
            credentials: 'include',
            method: 'DELETE'
        });

        Requests._checkRequestIsAuthorized(response);
        return response;
    }

    static async postFile(path: string, data: any) {
        const response = await fetch(Requests._linkTo(path), {
            method: 'POST',
            credentials: 'include',
            body: data
        });
        Requests._checkRequestIsAuthorized(response);
        return response;
    }


    static _linkTo(path: string): string {
        return apiOrigin + path;
    }

    static _checkRequestIsAuthorized(response: Response) {
        if (response.status === 401) {
            console.error('Endpoint returned error 401')
        }
    }
}
export default Requests;
