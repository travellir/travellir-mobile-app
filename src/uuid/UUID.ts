export function uuid(): string {
    const part1 = Math.random().toString(36).substring(2);
    const part2 = (new Date()).getTime().toString(36);
    return part1 + part2;
}