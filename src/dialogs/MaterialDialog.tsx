import React, { Component, ReactNode } from 'react';
import { Dialog } from 'react-native-material-ui';
import { Animated, Dimensions, View, StyleSheet, Platform, TouchableWithoutFeedback, KeyboardAvoidingView } from 'react-native';

const { width, height } = Dimensions.get('window');


interface Props {
    open: boolean,
    width?: number
    modal?: boolean
    onRequestClose: () => void
}

interface State {
    open: boolean
}

class MaterialDialog extends Component<Props, State> {

    dialogTranslate = new Animated.Value(-20);

    overlayOpacity = new Animated.Value(0);
    dialogOpacity = new Animated.Value(0);

    render() {
        const { onRequestClose, open, modal } = this.props;
        if (!open)
            return null;
        return (
            <View>
                {open &&
                    <TouchableWithoutFeedback onPress={!modal ? this._close : onRequestClose}>
                        <Animated.View style={[styles.overlay, { opacity: this.overlayOpacity }]} />
                    </TouchableWithoutFeedback>
                }
                <KeyboardAvoidingView style={[styles.dialogWrapper, { paddingBottom: 10 }]} behavior="position">
                    <Animated.View style={[{ opacity: this.dialogOpacity }, { transform: [{ translateY: this.dialogTranslate }] }]}>
                        <Dialog style={{ container: { width: this.props.width } }}>
                            <View style={{ marginTop: -24 }}>
                                {this.props.children}
                            </View>
                        </Dialog>
                    </Animated.View>
                </KeyboardAvoidingView>
            </View>
        )
    }
    openDialog = () => {
        Animated.parallel([
            Animated.timing(this.dialogOpacity, {
                duration: 150,
                toValue: 1,
                useNativeDriver: true
            }),
            Animated.timing(this.dialogTranslate, {
                duration: 150,
                toValue: 0,
                useNativeDriver: true
            }),
            Animated.timing(this.overlayOpacity, {
                duration: 150,
                toValue: .5,
                useNativeDriver: true
            })
        ]).start();
    }

    _close = () => {
        Animated.parallel([
            Animated.timing(this.dialogOpacity, {
                duration: 150,
                toValue: 0,
                useNativeDriver: true
            }),
            Animated.timing(this.dialogTranslate, {
                duration: 150,
                toValue: -20,
                useNativeDriver: true
            }),
            Animated.timing(this.overlayOpacity, {
                duration: 150,
                toValue: 0,
                useNativeDriver: true
            })
        ]).start(() => {
            this.props.onRequestClose();
        });
    }

    closeDialog = () => {
        Animated.parallel([
            Animated.timing(this.dialogOpacity, {
                duration: 150,
                toValue: 0,
                useNativeDriver: true
            }),
            Animated.timing(this.dialogTranslate, {
                duration: 150,
                toValue: -20,
                useNativeDriver: true
            }),
            Animated.timing(this.overlayOpacity, {
                duration: 150,
                toValue: 0,
                useNativeDriver: true
            })
        ]).start();
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.open === this.props.open)
            return;

        if (nextProps.open)
            this.openDialog();
        else
            this.closeDialog();
    }
};

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 0,
        height,
        width,
        backgroundColor: '#000'
    },
    dialogStyles: {
        padding: 0
    },
    dialogWrapper: {
        position: 'absolute',
        top: height * .2,
        left: 0,
        width,
        flexDirection: 'row',
        justifyContent: 'center',
        zIndex: 1
    }
})

export default MaterialDialog;