export interface DialogProps {
    open: boolean,
    onRequestClose: () => void
}