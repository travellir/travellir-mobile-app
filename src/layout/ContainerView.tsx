import React, { Component } from 'react';
import { View, StyleSheet, ViewStyle } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

interface Props {
    style?: ViewStyle
}

class ContainerView extends Component<Props> {
    render() {
        const externalStyle: ViewStyle = this.props.style || {};
        return (
            <View style={[styles.container, externalStyle]}>
                {this.props.children}
            </View>
        )
    }
};

export default ContainerView;