import React, { Component } from 'react';
import ContainerView from './ContainerView';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import { FlatList } from 'react-native';
import { uuid } from '../uuid/UUID';


interface Props {
    children: React.ReactElement<any>
    outerElements?: React.ReactNode
}

class ListScreen extends Component<Props> {
    render() {
        const { children, outerElements } = this.props;
        return (
            <ContainerView>
                {outerElements}
                <FlatList data={[1]} keyExtractor={(item, i) => uuid()} renderItem={() => children} />
            </ContainerView>
        )
    }
};

export default ListScreen;