import { AsyncStorage } from 'react-native';
import TravellirStore from '../TravellirStore';
import { actions } from './favorite-locations-redux';
import Requests from '../http/Requests';
import { LocationListItem } from '../travellir-map/MapTypes';

const favoriteLocationsKey = '@travellir/favorite-objects';

(async () => {
    try {
        const rawItem = await AsyncStorage.getItem(favoriteLocationsKey);
        const favoriteLocationIds: Array<number> = rawItem ? JSON.parse(rawItem) : [];
        TravellirStore.dispatch(actions.favoriteLocationsLoaded(favoriteLocationIds));
    }
    catch (err) {
        console.warn('Failed to load favorite locations.')
        TravellirStore.dispatch(actions.favoriteLocationsLoaded([]));
    }
})();

export async function saveFavoriteLocations(locationIds: Array<number>): Promise<void> {
    const json = JSON.stringify(locationIds);
    AsyncStorage.setItem(favoriteLocationsKey, json);
}

export default {

    async fetchLocations(locationIds: Array<number>) {
        if (locationIds.length === 0) {
            return;
        }

        const parameters = locationIds
            .map(locationId => `locationIds=${locationId}`)
            .join('&')

        const locations = (await Requests.jsonGet('/api/locations?' + parameters)) as Array<LocationListItem>;
        TravellirStore.dispatch(actions.favoriteLocationsFetched(locations));
    },

    markAsFavorite(locationId: number) {
        TravellirStore.dispatch(actions.markAsFavorite(locationId));
    },

    unmarkAsFavorite(locationId: number) {
        TravellirStore.dispatch(actions.unmarkAsFavorite(locationId));
    }

}