import { RawAction, TravellirModule } from '../travellir-redux';
import { saveFavoriteLocations } from './FavoriteLocationsService';
import { LocationListItem } from '../travellir-map/MapTypes';

export interface FavoriteLocationsState {
    favoriteLocations: {
        locationIds: Array<number>
        locations: Array<LocationListItem>
    }
}

export const types = {
    FAVORITE_LOCATIONS_FETCHED: 'FAVORITE_LOCATIONS_FETCHED',
    MARK_AS_FAVORITE: 'MARK_AS_FAVORITE',
    UNMARK_AS_FAVORITE: 'UNMARK_AS_FAVORITE',
    FAVORITE_LOCATIONS_READ: 'FAVORITE_LOCATIONS_READ'
}

export const actions = {
    favoriteLocationsFetched(locations: Array<LocationListItem>): RawAction<Array<LocationListItem>> {
        return {
            type: types.FAVORITE_LOCATIONS_FETCHED,
            payload: locations
        }
    },
    favoriteLocationsLoaded(locationIds: Array<number>): RawAction<Array<number>> {
        return {
            type: types.FAVORITE_LOCATIONS_READ,
            payload: locationIds
        }
    },
    markAsFavorite(locationId: number): RawAction<number> {
        return {
            type: types.MARK_AS_FAVORITE,
            payload: locationId
        }
    },
    unmarkAsFavorite(locationId: number): RawAction<number> {
        return {
            type: types.UNMARK_AS_FAVORITE,
            payload: locationId
        }
    }
}

export const FavoriteLocationsModule = {
    initialState: {
        favoriteLocations: {
            locationIds: [],
            locations: []
        }
    },
    reducers: {
        [types.FAVORITE_LOCATIONS_FETCHED](state: FavoriteLocationsState, fetchedLocations: Array<LocationListItem>): FavoriteLocationsState {
            const fetchedLocationIds = fetchedLocations.map(location => location.id)
            const locations = state.favoriteLocations.locations
                .filter(location => !fetchedLocationIds.includes(location.id))
                .concat(fetchedLocations);
            return {
                favoriteLocations: Object.assign({}, state.favoriteLocations, { locations })
            };
        },
        [types.FAVORITE_LOCATIONS_READ](state: FavoriteLocationsState, locationIds: Array<number>): FavoriteLocationsState {
            return {
                favoriteLocations: Object.assign({}, state.favoriteLocations, { locationIds })
            };
        },
        [types.MARK_AS_FAVORITE](state: FavoriteLocationsState, locationId: number): FavoriteLocationsState {
            const { locationIds } = state.favoriteLocations;
            if (locationIds.includes(locationId))
                return state;
            const updatedLocationIds = locationIds.concat([locationId]);
            saveFavoriteLocations(updatedLocationIds);
            return {
                favoriteLocations: Object.assign({}, state.favoriteLocations, {
                    locationIds: updatedLocationIds
                })
            };
        },
        [types.UNMARK_AS_FAVORITE](state: FavoriteLocationsState, locationId: number): FavoriteLocationsState {
            const { locationIds } = state.favoriteLocations;
            if (!locationIds.includes(locationId))
                return state;
            const updatedLocationIds = locationIds.filter(_locationId => locationId !== _locationId);
            saveFavoriteLocations(updatedLocationIds);
            return {
                favoriteLocations: Object.assign({}, state.favoriteLocations, {
                    locationIds: updatedLocationIds
                })
            };
        }
    },
} as TravellirModule<FavoriteLocationsState>;