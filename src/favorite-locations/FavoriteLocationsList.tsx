import React, { Component } from 'react';
import { View, FlatList, Dimensions, StyleSheet } from 'react-native';
import { FavoriteLocationsState } from './favorite-locations-redux';
import FavoriteLocationCard from './FavoriteLocationCard';
import { connectState } from '../travellir-redux';
import { LocationListItem } from '../travellir-map/MapTypes';

const { width, height } = Dimensions.get('window');

class FavoriteLocationsList extends Component<FavoriteLocationsState> {
    render() {
        const { locations, locationIds } = this.props.favoriteLocations;
        return (
            <View style={{ width, flexDirection: 'column', alignItems: 'center', paddingBottom: 10 }}>
                {this.locations(locations, locationIds)
                    .map(location =>
                        <FavoriteLocationCard location={location} key={`favorite-location-${location.id}`} />
                    )}
            </View>
        )
    }

    private locations(locations: LocationListItem[], locationIds: number[]) {
        return locations.filter(location => locationIds.includes(location.id));
    }
};

export default connectState(FavoriteLocationsList);