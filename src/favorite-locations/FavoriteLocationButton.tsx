import React, { Component } from 'react';
import FavoriteLocationsService from './FavoriteLocationsService';
import { connectState } from '../travellir-redux';
import { IconToggle } from 'react-native-material-ui';
import { FavoriteLocationsState } from './favorite-locations-redux';
import { LocationListItem } from '../travellir-map/MapTypes';
import { StyleSheet } from 'react-native';

interface Props extends FavoriteLocationsState {
    item: LocationListItem,
    big?: boolean
}

const styles = StyleSheet.create({
    saveLocationContainer: {
        width: 40,
        height: 40
    }
})

class FavoriteLocationButton extends Component<Props> {

    render() {
        const icon = this.isFavorite() ? 'favorite' : 'favorite-border';
        const { big } = this.props;
        return (
            <IconToggle
                style={big ? undefined : { container: styles.saveLocationContainer }}
                size={big ? 28 : 22}
                name={icon}
                onPress={this.saveLocation}
                color='#FA3D3D' />
        )
    }

    isFavorite = (): boolean => {
        const { favoriteLocations, item } = this.props;
        return (favoriteLocations.locationIds.includes(item.id));
    }

    saveLocation = async () => {
        const { favoriteLocations, item } = this.props;
        if (this.isFavorite())
            FavoriteLocationsService.unmarkAsFavorite(item.id);
        else
            FavoriteLocationsService.markAsFavorite(item.id);
    }
};

export default connectState(FavoriteLocationButton);