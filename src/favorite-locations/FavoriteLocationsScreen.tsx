import React, { PureComponent } from 'react';
import { connectState } from '../travellir-redux';
import { View, Image } from 'react-native';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import MenuButton from '../menu/MenuButton';
import CardHeader from '../material/CardHeader';
import FavoriteLocationsService from './FavoriteLocationsService';
import { FavoriteLocationsState } from './favorite-locations-redux';
import FavoriteLocationsList from './FavoriteLocationsList';
import ListScreen from '../layout/ListScreen';
import LoadingPage from '../material/LoadingPage';

const header = require('../../assets/favorite-locations-header.png');

interface State {
    loading: boolean
}

class FavoriteLocationsScreen extends PureComponent<FavoriteLocationsState, State> {

    state = {
        loading: true
    }
    
    render() {
        const { loading } = this.state;
        return (
            <ListScreen outerElements={<TravellirDrawer />}>
                <View>
                    <MenuButton />
                    <Image source={header} style={{ height: 220 }} />
                    <CardHeader text='Zapisane lokalizacje' />
                    {loading ? <LoadingPage /> : <FavoriteLocationsList />}
                </View>
            </ListScreen>
        )
    }

    componentWillReceiveProps(nextProps: FavoriteLocationsState) {
        const { favoriteLocations } = nextProps;
        this.fetchMissingLocations(favoriteLocations.locationIds);
    }

    componentDidMount() {
        this.setState({ loading: true })
        try {
            const { locationIds } = this.props.favoriteLocations;
            this.fetchMissingLocations(locationIds);
        }
        finally {
            this.setState({ loading: false })
        }
    }

    private fetchMissingLocations(locationIds: number[]) {
        const { locations } = this.props.favoriteLocations;
        const fetchedLocationIds = locations.map(location => location.id);
        const missingLocationIds = locationIds
            .filter(locationId => !fetchedLocationIds.includes(locationId));
        if (missingLocationIds.length != 0) {
            FavoriteLocationsService.fetchLocations(missingLocationIds);
        }
    }
};

export default connectState(FavoriteLocationsScreen);