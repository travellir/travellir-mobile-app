import React, { Component } from 'react';
import { LocationListItem } from '../travellir-map/MapTypes';
import { StyleSheet, Image, View, Text, Dimensions, TouchableNativeFeedback } from 'react-native';
import FavoriteLocationButton from './FavoriteLocationButton';
import { RippleFeedback, COLOR } from 'react-native-material-ui';
import LocationPageService from '../location-page/LocationPageService';

const { width, height } = Dimensions.get('window');

interface Props {
    location: LocationListItem
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#fff',
        borderRadius: 2,
        elevation: 2,
        marginVertical: 3,
        width: width * .95
    },
    contentContainer: {
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    locationTitleContainer: {
        flexDirection: 'column'
    },
    textLine: {
        textAlign: 'left',
        width: width * .6
    },
    locationTitle: {
        fontSize: 20,
        color: '#202020',
        marginBottom: 5,
    },
    locationAddress: {
        fontSize: 16,
        color: '#404040',
    }
});

class FavoriteLocationCard extends Component<Props> {
    render() {
        const { location } = this.props;
        return (
            <View style={styles.card}>
                <View style={styles.contentContainer}>
                    <RippleFeedback color={COLOR.amber300} onPress={this.navigateToLocationPage}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={{ uri: location.icon }} style={{ width: 50, marginHorizontal: 5 }} />
                            <View style={styles.locationTitleContainer}>
                                <Text numberOfLines={1} style={[styles.textLine, styles.locationTitle]}>{location.name}</Text>
                                <Text numberOfLines={1} style={[styles.textLine, styles.locationAddress]}>{location.address}</Text>
                            </View>
                        </View>
                    </RippleFeedback>
                    <FavoriteLocationButton item={location} />
                </View>
            </View>
        )
    }

    navigateToLocationPage = () => {
        const { location } = this.props;
        LocationPageService.openPage(location.id);
    }
};

export default FavoriteLocationCard;