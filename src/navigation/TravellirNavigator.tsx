import React from 'react';
import { StackNavigator, addNavigationHelpers, NavigationState } from "react-navigation";
import { connectRawState } from '../travellir-redux';
import { BackHandler } from 'react-native';
import NavigatorService from './NavigatorService';

import LocationsListScreen from '../travellir-map/locations-list/LocationsListScreen';
import LoginScreen from '../login-page/LoginScreen';
import TravellirMapScreen from '../travellir-map/TravellirMapScreen';
import FavoriteLocationsScreen from '../favorite-locations/FavoriteLocationsScreen';
import LocationPageScreen from '../location-page/LocationPageScreen';
import UserRoutesScreen from '../user-routes/UserRoutesScreen';
import UserRouteScreen from '../user-routes/assigned-locations/UserRouteScreen';
import LocationSearchScreen from '../locations-search/LocationSearchScreen';
import CameraRollScreen from '../location-page/send-location-photo/CameraRollScreen';
import TakePictureScreen from '../location-page/send-location-photo/TakePictureScreen';
import SentPhotosScreen from '../sent-photos/SentPhotosScreen';
import LocationPhotosScreen from '../sent-photos/location-photos/LocationPhotosScreen';
import PhotoScreen from '../sent-photos/location-photos/PhotoScreen';
import LocalizationService from '../localization/LocalizationService';
import CustomMapScreen from '../travellir-map/custom-map/CustomMapScreen';
import SendRouteScreen from '../user-routes/route-synchronization/SendRouteScreen';
import ReadRouteScreen from '../user-routes/route-synchronization/ReadRouteScreen';

export const TravellirNavigator = StackNavigator(
    {
        Login: {
            screen: LoginScreen
        },
        TravellirMapScreen: {
            screen: TravellirMapScreen
        },
        LocationsListScreen: {
            screen: LocationsListScreen
        },
        FavoriteLocationsScreen: {
            screen: FavoriteLocationsScreen
        },
        LocationPageScreen: {
            screen: LocationPageScreen
        },
        UserRoutesScreen: {
            screen: UserRoutesScreen
        },
        UserRouteScreen: {
            screen: UserRouteScreen
        },
        LocationSearchScreen: {
            screen: LocationSearchScreen
        },
        CameraRollScreen: {
            screen: CameraRollScreen
        },
        TakePictureScreen: {
            screen: TakePictureScreen
        },
        SentPhotosScreen: {
            screen: SentPhotosScreen
        },
        LocationPhotosScreen: {
            screen: LocationPhotosScreen
        },
        PhotoScreen: {
            screen: PhotoScreen
        },
        CustomMapScreen: {
            screen: CustomMapScreen
        },
        SendRouteScreen: {
            screen: SendRouteScreen
        },
        ReadRouteScreen: {
            screen: ReadRouteScreen
        }
    },
    {
        headerMode: 'none'
    });

interface Props {
    navigation: NavigationState,
    dispatch: () => any;
}

const EVENT_SUBSCRIPTION = {
    remove: () => { }
};
class TravellirNavigation extends React.Component<Props> {

    componentDidMount() {
        LocalizationService.askPersmission();
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress = () => {
        const { dispatch, navigation } = this.props;
        if (navigation.index === 0) {
            return false;
        }
        NavigatorService.goBack();
        return true;
    };

    render() {
        const { dispatch, navigation } = this.props;
        return (
            <TravellirNavigator navigation={addNavigationHelpers({
                dispatch,
                state: { ...navigation, key: 'StackRouterRoot' },
                addListener: () => EVENT_SUBSCRIPTION
            })} />
        )
    }

};

export default connectRawState(TravellirNavigation);