import { TravellirNavigator } from './TravellirNavigator';


const navigationAction = TravellirNavigator.router.getActionForPathAndParams('TravellirMapScreen');
if (navigationAction == null)
    throw new Error();

const navigationInitialState = TravellirNavigator.router.getStateForAction(navigationAction);

export const navigationReducer = <T>(state: any = navigationInitialState, action: any) => {
    return TravellirNavigator.router.getStateForAction(action, state) || state;
}