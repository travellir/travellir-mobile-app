import TravellirStore from '../TravellirStore';
import { NavigationActions } from 'react-navigation';


export default {
    navigateTo(screen: string) {
        const action = NavigationActions.navigate({
            routeName: screen,
            key: screen
        });
        TravellirStore.dispatch(action);
    },
    goBack() {
        TravellirStore.dispatch(NavigationActions.back());
    }
};