import { TravellirReducers } from './travellir-redux';
import { LoginPageState, LoginPageModule } from './login-page/login-page-redux';
import { TravellirMapState, TravellirMapModule } from './travellir-map/travellir-map-redux';
import { DrawerModule } from './travellir-drawer/travellir-drawer-redux';
import { FavoriteLocationsModule } from './favorite-locations/favorite-locations-redux';
import { LocationPageModule } from './location-page/location-page-redux';
import { UserRoutesModule } from './user-routes/user-routes-redux';
import { LocationsSearchModule } from './locations-search/locations-search-redux';
import { SentPhotosModule } from './sent-photos/sent-photos-redux';
import { LocalizationModule } from './localization/localization-redux';

const initialState = {
    ...LoginPageModule.initialState,
    ...TravellirMapModule.initialState,
    ...DrawerModule.initialState,
    ...FavoriteLocationsModule.initialState,
    ...LocationPageModule.initialState,
    ...UserRoutesModule.initialState,
    ...LocationsSearchModule.initialState,
    ...SentPhotosModule.initialState,
    ...LocalizationModule.initialState

};
const reducers = {
    ...LoginPageModule.reducers,
    ...TravellirMapModule.reducers,
    ...DrawerModule.reducers,
    ...FavoriteLocationsModule.reducers,
    ...LocationPageModule.reducers,
    ...UserRoutesModule.reducers,
    ...LocationsSearchModule.reducers,
    ...SentPhotosModule.reducers,
    ...LocalizationModule.reducers
}

export const travellirReducer = <T>(state: any = initialState, action: any): any => {
    const reducer = reducers[action.type];
    if (reducer) {
        const stateChange = reducer(state, action.payload);
        return Object.assign({}, state, stateChange);
    }
    return state;
}