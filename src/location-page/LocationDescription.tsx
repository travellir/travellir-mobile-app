import React, { Component, BlockquoteHTMLAttributes } from 'react';
import { LocationDescription, Block, ParagraphConfiguration, ExternalImageConfiguration } from './location-page-types';
import { Text, Image, StyleSheet, View, Dimensions } from 'react-native';


const { width, height } = Dimensions.get('window');

interface Props {
    description: LocationDescription
}

class LocationDescriptionView extends Component<Props> {
    render() {
        const { description } = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: '#dfdfdf', width, flexDirection: 'column', alignItems: 'center', paddingVertical: 5 }}>
                {description.children.map(this.asComponent)}
            </View>
        )
    }

    asComponent = (block: Block) => {
        switch (block.model_configuration.type) {
            case 'PARAGRAPH':
                return <Paragraph key={block.uuid} model={block.model_configuration as ParagraphConfiguration} />
            case 'EXTERNAL_IMAGE':
                return <ExternalImage key={block.uuid} model={block.model_configuration as ExternalImageConfiguration} />
            default:
                throw new Error('No such block type [' + block.model_configuration.type + ']');
        }
    }

};

const cardHeight = width * .96;
const paragraphStyles = StyleSheet.create({
    cardStyles: {
        padding: 12,
        backgroundColor: '#fff',
        borderRadius: 2,
        elevation: 4,
        marginVertical: 3,
        width: cardHeight
    },
    paragraph: {
        fontSize: 18,
        textAlign: 'justify',
        lineHeight: 28,
        color: '#252525'
    }
})
interface ParagraphProps {
    model: ParagraphConfiguration
}
class Paragraph extends React.Component<ParagraphProps> {

    render() {
        const { model } = this.props;
        return (
            <View style={paragraphStyles.cardStyles}>
                <Text style={paragraphStyles.paragraph}>{model.text}</Text>
            </View>
        )
    }
}


interface ExternalImageProps {
    model: ExternalImageConfiguration
}
class ExternalImage extends React.Component<ExternalImageProps> {

    render() {
        const { model } = this.props;
        return (<Image source={{ uri: model.image_url }} style={{ height: 250, width, marginVertical: 5 }} />)
    }
}

export default LocationDescriptionView;