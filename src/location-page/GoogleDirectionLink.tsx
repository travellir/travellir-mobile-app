import * as React from 'react';
import { View, StyleSheet, Text, Linking, } from 'react-native';
import { Divider, IconToggle, COLOR, ActionButton } from 'react-native-material-ui';
import { Card } from 'react-native-material-ui';
import { LocationListItem } from '../travellir-map/MapTypes';
import { Button } from 'react-native-material-ui';
import { RippleFeedback } from 'react-native-material-ui';

interface Props {
    location: LocationListItem
}

const styles = StyleSheet.create({
    cardView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    address: {
        fontSize: 18,
    }
})

class GoogleDirectionLink extends React.PureComponent<Props, any> {
    render() {
        const { location } = this.props;
        return (

            <View>
                <Card style={{ container: { width: '100%' } }}>
                    <RippleFeedback color={COLOR.blue200}>
                        <View style={styles.cardView}>
                            <IconToggle
                                onPress={this.navigate}
                                style={{
                                    container: {
                                        backgroundColor: COLOR.blue500,
                                        borderRadius: 50,
                                        width: 40,
                                        height: 40,
                                        margin: 10
                                    }
                                }}
                                name="navigation"
                                color={COLOR.white} />
                            <Text style={styles.address}>{location.address}</Text>
                        </View>
                    </RippleFeedback>
                </Card>
            </View>
        );
    }

    navigate = () => {
        const { location } = this.props;
        const desinationCoords = `${location.coords.lat},${location.coords.lng}`;
        const link = `https://www.google.com/maps/dir/?api=1&` + `destination=${desinationCoords}`;
        Linking.openURL(link)
    }
}

export default GoogleDirectionLink;