import React, { Component } from 'react';
import { Image, View, Text, Dimensions, StyleSheet } from 'react-native';
import LocationRouteAssigmentsForm from './LocationRouteAssigmentsForm';
import MaterialDialog from '../../dialogs/MaterialDialog';
import { DialogProps } from '../../dialogs/DialogProperties';
import { LocationListItem } from '../../travellir-map/MapTypes';
import { Button } from 'react-native-material-ui';
import { connectState } from '../../travellir-redux';
import { UserRoutesState } from '../../user-routes/user-routes-redux';
import UserRoutesService from '../../user-routes/UserRoutesService';

interface Props extends DialogProps, UserRoutesState {
    location: LocationListItem
}

const { width } = Dimensions.get('window');
const dialogWidth = width * .9;

interface State {
    assignments: string[]
}
class RouteAssignmentsDialog extends Component<Props, State> {
    private dialog: MaterialDialog | null = null;

    constructor(props: Props) {
        super(props);
        this.state = {
            assignments: this.locationAssignments(props)
        }
    }

    render() {
        const { open, onRequestClose, location } = this.props;
        const { assignments } = this.state;
        return (
            <MaterialDialog
                width={dialogWidth}
                open={open}
                onRequestClose={this.onRequestClose}>
                <RouteAssignmentsDialogHeader />
                <LocationRouteAssigmentsForm
                    onAssignmentsChanged={(assignments: string[]) => this.setState({ assignments })}
                    assignments={assignments}
                    location={location} />
                <View style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between' }}>
                    <Button onPress={this.onRequestClose} style={{ text: { color: '#2A7EE0' } }} text={'Zamknij'} />
                    <Button onPress={this.onSave} style={{ text: { color: '#2A7EE0' } }} text={'Zapisz'} />
                </View>
            </MaterialDialog>
        )
    }

    locationAssignments = (props: Props): string[] => {
        const { userRoutes, location } = this.props;

        return userRoutes.routes
            .filter(route => route.locationIds.includes(location.id))
            .map(route => route.id);
    }

    onSave = async () => {
        const { location, userRoutes, currentRoute } = this.props;
        const { assignments } = this.state;

        const updatedRoutes = await UserRoutesService.saveAssignments(location.id, assignments, userRoutes.routes);
        this.onRequestClose();

        const selectedRoute = currentRoute.route;
        if (selectedRoute) {
            const foundRoute = updatedRoutes.find(route => route.id === selectedRoute.id);
            if (foundRoute) {
                await UserRoutesService.currentRouteFetched(foundRoute)
                UserRoutesService.fetchLocations(foundRoute);
            }
        }
    }

    onRequestClose = () => {
        this.setState({
            assignments: this.locationAssignments(this.props)
        });
        this.props.onRequestClose();
    }

    close = () => {
        if (this.dialog) {
            this.dialog._close();
        }
    }
};


const headerImage = require('../../../assets/location-assignments-header.png');
const travellirLogo = require('../../../assets/travellir-logo.png');
const headerStyles = StyleSheet.create({
    stickBottom: {
        position: 'absolute',
        width: dialogWidth,
        bottom: 0
    },
    container: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
})
class RouteAssignmentsDialogHeader extends React.Component {

    render() {
        return (
            <View>
                <Image borderTopLeftRadius={2} borderTopRightRadius={2} source={headerImage} style={{ height: 160 }} />
                <View style={[headerStyles.stickBottom, headerStyles.container]}>
                    <View style={{ width: dialogWidth * .75 }}>
                        <Text style={{ color: '#fff', fontSize: 25 }}>Dodaj obiekt do swoich tras</Text>
                    </View>
                    <View>
                        <Image source={travellirLogo} style={{ width: 60, height: 60 }} />
                    </View>
                </View>
            </View>
        )
    }
}

export default connectState(RouteAssignmentsDialog);