import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connectState } from '../../travellir-redux';
import { UserRoutesState } from '../../user-routes/user-routes-redux';
import { Checkbox } from 'react-native-material-ui';
import { LocationListItem } from '../../travellir-map/MapTypes';
import { UserRoute } from '../../user-routes/user-routes-types';

interface Props extends UserRoutesState {
    location: LocationListItem,
    assignments: string[],
    onAssignmentsChanged: (assignments: string[]) => void
}

class LocationRouteAssigmentsForm extends Component<Props> {

    render() {
        const { userRoutes, location, assignments } = this.props;
        return (
            <View>
                <ScrollView style={{ maxHeight: 220 }} contentContainerStyle={{ padding: 10 }}>
                    {userRoutes.routes.map((route, i) =>
                        <View key={`user-route-${i}`}>
                            <Checkbox
                                style={{ container: { marginBottom: -10 }, icon: { color: '#2A7EE0' } }}
                                onCheck={() => this.assignToRoute(route)}
                                label={route.name}
                                value={1}
                                checked={assignments.includes(route.id)} />
                        </View>
                    )}
                </ScrollView>
            </View>
        )
    }

    assignToRoute = (route: UserRoute) => {
        const { location, assignments, onAssignmentsChanged } = this.props;
        
        if (assignments.includes(route.id)) {
            const updatedAssignments = assignments.filter(routeId => routeId !== route.id);
            onAssignmentsChanged(updatedAssignments)
        }
        else {
            const updatedAssignments = assignments.concat([route.id]);
            onAssignmentsChanged(updatedAssignments)
        }
        
    }
};

export default connectState(LocationRouteAssigmentsForm);