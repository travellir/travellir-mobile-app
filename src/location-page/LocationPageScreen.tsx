import React, { Component } from 'react';
import { LocationPageState } from './location-page-redux';
import { connectState } from '../travellir-redux';
import { View, StyleSheet, Text, Image, FlatList, Dimensions, ActivityIndicator } from 'react-native';
import LocationPageService from './LocationPageService';
import PageHeader from './PageHeader';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import LocationDescriptionView from './LocationDescription';
import { MenuProvider } from 'react-native-popup-menu';
import LocationPageMenu from './LocationPageMenu';
import RouteAssignmentsDialog from './route-assignments/RouteAssignmentsDialog';
import ChoosePhotoSourceDialog from './send-location-photo/ChoosePhotoSourceDialog';
import GoogleDirectionLink from './GoogleDirectionLink';
import { COLOR } from 'react-native-material-ui';
import LoadingPage from '../material/LoadingPage';

interface Props extends LocationPageState {
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

class LocationPageScreen extends React.PureComponent<Props> {

    state = {
        assignmentsDialogOpen: false,
        sendPhotoDialogOpen: false
    }

    render() {
        const {
            locationDescription,
            locationPage,
            locationLoading,
            requestedLocationId
        } = this.props;

        if (!locationPage || requestedLocationId !== locationPage.id) {
            return <LoadingPage />;
        }

        return (
            <MenuProvider>
                <TravellirDrawer />
                <RouteAssignmentsDialog
                    location={locationPage}
                    open={this.state.assignmentsDialogOpen}
                    onRequestClose={() => this.setState({ assignmentsDialogOpen: false })} />
                <ChoosePhotoSourceDialog
                    open={this.state.sendPhotoDialogOpen}
                    onRequestClose={() => this.setState({ sendPhotoDialogOpen: false })}
                />
                <FlatList
                    data={[1]}
                    keyExtractor={(item, index) => `item-${index}`}
                    renderItem={() =>
                        <View style={{ minHeight: Dimensions.get('window').height }}>
                            <PageHeader
                                contextMenu={
                                    <LocationPageMenu
                                        onAssignmentsSelected={() => this.setState({ assignmentsDialogOpen: true })}
                                        onSendPhotoSelected={() => this.setState({ sendPhotoDialogOpen: true })}

                                    />}
                                locationPage={locationPage} />
                            {locationDescription && <LocationDescriptionView description={locationDescription} />}
                            <GoogleDirectionLink location={locationPage} />
                        </View>
                    } />
            </MenuProvider>
        )
    }

    componentDidMount() {
        const { locationPage, locationLoading, requestedLocationId } = this.props;
        if (requestedLocationId) {
            LocationPageService.fetchLocation(requestedLocationId);
            LocationPageService.fetchDescription(requestedLocationId);
        }
    }
};

export default connectState(LocationPageScreen);