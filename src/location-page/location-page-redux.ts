import { RawAction, TravellirModule } from '../travellir-redux';
import { LocationPage, LocationDescription } from './location-page-types';

export interface LocationPageState {
    requestedLocationId: number | null,
    locationLoading: boolean,
    locationPage: LocationPage | null,
    locationDescription: LocationDescription | null
}


export const types = {
    LOCATION_PAGE_FETCHED: 'LOCATION_PAGE_FETCHED',
    LOCATION_DESCRIPTION_FETCHED: 'LOCATION_DESCRIPTION_FETCHED',
    REQUEST_LOCATION_PAGE: 'REQUEST_LOCATION_PAGE'
}

export const actions = {
    locationDescriptionFetched(locationDescription: LocationDescription): RawAction<LocationDescription> {
        return {
            type: types.LOCATION_DESCRIPTION_FETCHED,
            payload: locationDescription
        }
    },
    locationPageFetched(locationPage: LocationPage): RawAction<LocationPage> {
        return {
            type: types.LOCATION_PAGE_FETCHED,
            payload: locationPage
        }
    },
    requestLocationPage(locationId: number): RawAction<number> {
        return {
            type: types.REQUEST_LOCATION_PAGE,
            payload: locationId
        }
    }
}

export const LocationPageModule = {
    initialState: {
        requestedLocationId: null,
        locationLoading: false,
        descriptionLoading: false,
        locationDescription: null,
        locationPage: null
    },
    reducers: {
        [types.LOCATION_DESCRIPTION_FETCHED](state: LocationPageState, payload: LocationDescription) {
            return {
                descriptionLoading: false,
                locationDescription: payload
            }
        },
        [types.LOCATION_PAGE_FETCHED](state: LocationPageState, payload: LocationPage) {
            return {
                requestedLocationId: payload.id,
                locationLoading: false,
                locationPage: payload
            }
        },
        [types.REQUEST_LOCATION_PAGE](state: LocationPageState, locationId: number) {
            return {
                requestedLocationId: locationId,
                locationLoading: false,
                locationPage: null,
                descriptionLoading: false,
                locationDescription: null
            }
        }
    }
} as TravellirModule<LocationPageState>