import * as React from 'react';
import { View, StyleSheet, Text, Dimensions, ActivityIndicator, } from 'react-native';
import MaterialDialog from '../../dialogs/MaterialDialog';

export interface Props {
    photoChosen: boolean,
    photoUploaded: boolean
}

class PhotoSendDialog extends React.PureComponent<Props, any> {
    render() {
        const { photoChosen, photoUploaded } = this.props;
        return (
            <MaterialDialog width={Dimensions.get('window').width * .8} modal={true} open={photoChosen || photoUploaded} onRequestClose={() => { }}>
                <View style={{ padding: 20 }}>
                    {photoChosen &&
                        <View style={{ flexDirection: 'column', justifyContent: 'space-around' }}>
                            <Text>Zdjęcie zostało wybrane. Właśnie wysyłamy je na nasze serwery.</Text>
                            <Text style={{ marginBottom: 20 }}>Czekaj cierpliwie...</Text>
                            <ActivityIndicator animating={true} size={40} />
                        </View>}
                    {photoUploaded &&
                        <View style={{ flexDirection: 'column', justifyContent: 'space-around' }}>
                            <Text>Zdjęcie zostało pomyślnie wysłane.</Text>
                            <Text>Kto wie, może niedługo zobaczysz je w galerii obiektu?</Text>
                        </View>}
                </View>
            </MaterialDialog>
        );
    }
}

export default PhotoSendDialog;