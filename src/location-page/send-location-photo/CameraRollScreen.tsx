import * as React from 'react';
import { View, StyleSheet, Text, CameraRoll, ActivityIndicator, Dimensions } from 'react-native';
import { Permissions, ImagePicker } from 'expo';
import firebase from 'firebase';
import { uuid } from '../../uuid/UUID';
import MaterialDialog from '../../dialogs/MaterialDialog';
import NavigatorService from '../../navigation/NavigatorService';
import { connectState } from '../../travellir-redux';
import { LocationPageState } from '../location-page-redux';
import { LoginPageState } from '../../login-page/login-page-redux';
import PhotoSendDialog from './PhotoSendDialog';
import SendPhotoService from './SendPhotoService';
import ImageResizeService from './ImageResizeService';

interface CameraRollScreenProps extends LocationPageState, LoginPageState {
}

class CameraRollScreen extends React.PureComponent<CameraRollScreenProps, any> {

    state = {
        photoChosen: false,
        photoUploaded: false
    }

    render() {
        const { photoChosen, photoUploaded } = this.state;
        return (
            <View>
                <PhotoSendDialog photoChosen={photoChosen} photoUploaded={photoUploaded} />
            </View>
        );
    }

    async componentDidMount() {
        const { locationPage, loggedUser } = this.props;
        if (!locationPage || !loggedUser) {
            NavigatorService.goBack();
            return;
        }

        const result = await ImagePicker.launchImageLibraryAsync();
        if (result.cancelled) {
            NavigatorService.goBack();
            return;
        }

        this.setState({ photoChosen: true });

        const thumbnail = await ImageResizeService.resize(result, { width: 300, height: 300 });

        await SendPhotoService.sendPhoto(locationPage.id, loggedUser, { original: result.uri, thumbnail: thumbnail });
        this.setState({ photoChosen: false, photoUploaded: true });
    }
}

export default connectState(CameraRollScreen);