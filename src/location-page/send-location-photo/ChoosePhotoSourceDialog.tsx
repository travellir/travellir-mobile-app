import * as React from 'react';
import { View, StyleSheet, Text, Dimensions, Image, } from 'react-native';
import { DialogProps } from '../../dialogs/DialogProperties';
import MaterialDialog from '../../dialogs/MaterialDialog';
import { RippleFeedback, Icon, Divider } from 'react-native-material-ui';
import { COLOR } from 'react-native-material-ui';
import NavigatorService from '../../navigation/NavigatorService';
import { LoginPageState } from '../../login-page/login-page-redux';
import { connectState } from '../../travellir-redux';

export interface Props extends DialogProps, LoginPageState {
}
const { width } = Dimensions.get('window');

const header = require('../../../assets/photos-header.png');

class ChoosePhotoSourceDialog extends React.PureComponent<Props, any> {
    render() {
        const { open, onRequestClose, loggedUser } = this.props;
        return (
            <MaterialDialog width={width * .9} open={open} onRequestClose={onRequestClose}>
                <View>
                    <Image source={header} />
                    <View style={{ position: 'absolute', bottom: 0, padding: 15 }}>
                        <Text style={{ color: '#fff', fontSize: 24 }}>Wyślij zdjęcie obiektu</Text>
                    </View>
                </View>
                {loggedUser ?
                    <React.Fragment>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 20 }}>
                            <View style={{ width: width * .55, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <PhotoSourceOption onPress={async () => this.navigateTo('CameraRollScreen')} icon='insert-photo' label='Galeria' />
                                <PhotoSourceOption onPress={async () => this.navigateTo('TakePictureScreen')} icon='photo-camera' label='Aparat' />
                            </View>
                        </View>
                        <Divider />
                        <View style={{ padding: 10 }}>
                            <Text style={{ color: COLOR.grey700, lineHeight: 22 }}>Wysłane zdjęcie możesz skasować w dowolnym momencie.</Text>
                            <Text style={{ color: COLOR.grey700, lineHeight: 22 }}>Pamiętaj jednak, że zdjęcie nie zniknie jeśli właściciel obiektu włączy je do swojej galerii.</Text>
                        </View>
                    </React.Fragment> :
                    <View style={{ padding: 10 }}>
                        <Text style={{ color: COLOR.grey700, lineHeight: 22 }}>Wysyłanie zdjęć jest dostępne tylko dla zalogowanych użytkowników.</Text>
                    </View>
                }
            </MaterialDialog>
        );
    }

    navigateTo = (screen: string) => {
        NavigatorService.navigateTo(screen);
        this.props.onRequestClose();
    }
}


interface PhotoSourceOptionProps {
    icon: string,
    label: string,
    onPress?: () => void
}
class PhotoSourceOption extends React.Component<PhotoSourceOptionProps> {
    render() {
        const { icon, label, onPress } = this.props;
        return (
            <RippleFeedback onPress={onPress} color={COLOR.lightBlueA100} borderless={true}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                    <Icon name={icon} color="#6EA3FA" size={40} />
                    <Text style={{ fontSize: 20, marginTop: 5 }}>{label}</Text>
                </View>
            </RippleFeedback>
        );
    }
}

export default connectState(ChoosePhotoSourceDialog);