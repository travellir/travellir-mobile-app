import * as React from 'react';
import { View, StyleSheet, Text, ActivityIndicator, } from 'react-native';
import { Permissions, Camera } from 'expo';
import { BorderCollapseProperty } from 'csstype';
import TakePictureButton from './TakePictureButton';
import { IconToggle } from 'react-native-material-ui';
import { COLOR } from 'react-native-material-ui';
import firebase from 'firebase';
import { LocationPageState } from '../location-page-redux';
import { LoginPageState } from '../../login-page/login-page-redux';
import { connectState } from '../../travellir-redux';
import { uuid } from '../../uuid/UUID';
import PhotoSendDialog from './PhotoSendDialog';
import SendPhotoService from './SendPhotoService';
import ImageResizeService from './ImageResizeService';

interface Props extends LocationPageState, LoginPageState {
}

interface State {
    cameraPermissionGranted: boolean,
    cameraType: number,
    autoFlash: boolean,
    cameraReady: boolean
    photoChosen: boolean,
    photoUploaded: boolean
}

const styles = StyleSheet.create({
    toolbar: {
        flex: .16
    },
    cameraContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    darkBackground: {
        backgroundColor: '#000',
    }
});

class TakePictureScreen extends React.PureComponent<Props, State> {

    state = {
        cameraPermissionGranted: false,
        cameraType: 0,
        autoFlash: true,
        cameraReady: false,
        photoChosen: false,
        photoUploaded: false
    }

    private camera: Camera | null = null;

    render() {
        const {
            cameraType,
            cameraPermissionGranted,
            autoFlash,
            cameraReady,
            photoChosen,
            photoUploaded } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <PhotoSendDialog photoChosen={photoChosen} photoUploaded={photoUploaded} />
                <View style={[{ flex: .84 }, styles.darkBackground]}>
                    {cameraPermissionGranted &&
                        <Camera
                            onCameraReady={() => this.setState({ cameraReady: true })}
                            ref={(_ref: any) => { this.camera = _ref }}
                            flashMode={autoFlash ? 3 /* auto */ : 0 /* off */}
                            type={cameraType}
                            style={{ flex: 1 }}>

                        </Camera>}
                </View>
                <View style={[styles.toolbar, styles.cameraContainer, styles.darkBackground]}>
                    <CameraActionButton
                        onEnable={() => this.setState({ cameraType: 1 })}
                        onDisable={() => this.setState({ cameraType: 0 })}
                        enabled={cameraType == 1}
                        icon={'camera-front'} />
                    <TakePictureButton onPress={this.takePicture} />
                    <CameraActionButton
                        onEnable={() => this.setState({ autoFlash: true })}
                        onDisable={() => this.setState({ autoFlash: false })}
                        enabled={autoFlash}
                        icon={'flash-auto'} />
                </View>
            </View>
        );
    }

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        if (status !== 'granted') {
            console.log('Odmówiono uprawnień do kamery')
            return;
        }
        this.setState({ cameraPermissionGranted: true })
    }

    takePicture = async () => {
        const { locationPage, loggedUser } = this.props;
        if (!this.camera || !locationPage || !loggedUser)
            return;

        const result = await this.camera.takePictureAsync({
            width: 1080,
            skipProcessing: true,
            quality: 0,
            fixOrientation: false
        });
        this.setState({
            photoChosen: true,
            photoUploaded: false
        })
        const thumbnail = await ImageResizeService.resize(result, { width: 300, height: 300 });

        SendPhotoService.sendPhoto(locationPage.id, loggedUser, { original: result.uri, thumbnail: thumbnail })
        this.setState({
            photoChosen: false,
            photoUploaded: true
        });
    }
}

interface CameraActionButtonProps {
    icon: string,
    enabled: boolean
    onEnable?: () => void
    onDisable?: () => void
}

class CameraActionButton extends React.PureComponent<CameraActionButtonProps> {

    render() {
        const { icon, enabled } = this.props;
        return (
            <IconToggle onPress={this.buttonPressed} name={icon} color={enabled ? COLOR.blue400 : COLOR.white} size={30} />
        )
    }

    buttonPressed = () => {
        const { enabled, onEnable, onDisable } = this.props;
        if (enabled && onDisable)
            onDisable();
        else if (!enabled && onEnable)
            onEnable();
    }
}

export default connectState(TakePictureScreen);