import { TravellirUser } from '../../login-page/authentication-types';
import firebase from 'firebase';
import { uuid } from '../../uuid/UUID';

export default {
    async sendPhoto(locationId: number, user: TravellirUser, uris: { original: string, thumbnail: string }) {
        const originalBlob = await fetch(uris.original);
        const thumbnailBlob = await fetch(uris.thumbnail);

        const rootRef = firebase
            .storage()
            .ref();
        const imageId = uuid();

        const originalRef = rootRef
            .child('sent-photos')
            .child(imageId);
        const thumbnailRef = rootRef
            .child('sent-photos-thumbnails')
            .child(imageId);



        const originalSnapshot = originalRef.put(await originalBlob.blob());
        const thumbnailSnapshot = thumbnailRef.put(await thumbnailBlob.blob());

        const uploadedThumbnail = (await thumbnailSnapshot);
        firebase.database()
            .ref()
            .child('sent-photos')
            .push({
                thumbnail: uploadedThumbnail.ref.fullPath,
                thumbnailUrl: (await uploadedThumbnail.ref.getDownloadURL()),
                original: (await originalSnapshot).ref.fullPath,
                userUid: user.uid,
                locationId
            });
    }
}