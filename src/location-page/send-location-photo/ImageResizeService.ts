import { ImageEditor } from 'react-native';


export default {
    async resize(image: { uri: string, width: number, height: number }, params: { width: number, height: number }): Promise<string> {
        return new Promise((resolve: (uri: string) => void, reject) => {
            ImageEditor.cropImage(image.uri,
                {
                    offset: { x: 0, y: 0 },
                    size: { width: image.width, height: image.height },
                    displaySize: { width: 200, height: 400 },
                    resizeMode: 'contain',
                },
                (uri) => resolve(uri),
                () => reject(),
            );
        });
    }
}