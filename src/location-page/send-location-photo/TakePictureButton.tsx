import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { RippleFeedback, COLOR } from 'react-native-material-ui';

interface Props {
    onPress: () => void
}

const styles = StyleSheet.create({
    circle: {
        borderRadius: 50
    },
    bigCircle: {
        width: 70,
        height: 70,
        backgroundColor: '#dfdfdf'
    },
    smallCircle: {
        width: 55,
        height: 55,
        backgroundColor: '#303030'
    },
    centerContent: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

class TakePictureButton extends React.PureComponent<Props, any> {
    render() {
        const { onPress } = this.props;
        return (
            <View style={[styles.circle, styles.bigCircle, styles.centerContent]}>
                <RippleFeedback onPress={onPress} color={COLOR.orangeA200}>
                    <View style={[styles.circle, styles.smallCircle]}>

                    </View>
                </RippleFeedback>
            </View>
        );
    }
}

export default TakePictureButton;