import React, { Component } from 'react';
import { LocationPage } from './location-page-types';
import { View, Dimensions, StyleSheet, Image, Text } from 'react-native';
import MenuButton from '../menu/MenuButton';
import LocationGalleryCarousel from './LocationGalleryCarousel';
import BigFavoriteLocationButton from './BigFavoriteLocationButton';

const { width, height } = Dimensions.get('window');

interface Props {
    locationPage: LocationPage
    contextMenu?: React.ReactNode
}

const styles = StyleSheet.create({
    image: {

    },
    stickyBottom: {
        width: width,
        position: 'absolute',
        bottom: 0
    },
    cardTitle: {
        paddingHorizontal: 12,
        paddingVertical: 20,
        backgroundColor: 'rgba(0, 0, 0, .75)'
    },
    textLine: {
        textAlign: 'left'
    },
    title: {
        color: '#f0f0f0',
        fontSize: 24,
        marginBottom: 3,
    },
    secondaryTitle: {
        color: '#dadada',
        fontSize: 18,
    },
    alignEnd: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    verticalColumn: {
      flexDirection: 'column',
      alignItems: 'center',

    }
})

class PageHeader extends Component<Props> {
    render() {
        const { locationPage } = this.props;
        return (
            <View>
                <MenuButton />
                {this.props.contextMenu}
                <LocationGalleryCarousel locationPage={locationPage} />
                <View style={styles.stickyBottom}>
                    <View style={[styles.alignEnd, { width }]}>
                        <View style={[styles.verticalColumn]}>
                            <Image source={{ uri: locationPage.icon }} style={{ height: 80, width: 80 }} />
                            <BigFavoriteLocationButton item={locationPage} />
                        </View>
                    </View>
                    <View style={[styles.cardTitle]}>
                        <Text numberOfLines={1} style={[styles.textLine, styles.title]}>{locationPage.name}</Text>
                        <Text numberOfLines={1} style={[styles.textLine, styles.secondaryTitle]}>{locationPage.address}</Text>
                    </View>
                </View>
            </View>
        )
    }
};

export default PageHeader;