import { LocationListItem } from '../travellir-map/MapTypes';
import { InteractionManager } from 'react-native';


export interface GalleryImage {
    id: number,
    name: string,
    size: number,
    extension: string,
    preview: string
}

export interface LocationGallery {
    image_ids: number[],
    images: GalleryImage[]
}

export interface LocationPage extends LocationListItem {
    gallery: LocationGallery
}

export interface LocationDescription {
    id: string,
    location_id: number,
    children: Blocks
}

type Blocks = Array<Block>;

export interface Block {
    children: Block[],
    uuid: string,
    model_configuration: ModelConfiguration
}

export interface ModelConfiguration {
    type: 'EXTERNAL_IMAGE' | 'PARAGRAPH'
}

export interface ParagraphConfiguration extends ModelConfiguration {
    text: string
}

export interface ExternalImageConfiguration extends ModelConfiguration {
    image_url: string
}