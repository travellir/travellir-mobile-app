import React, { Component } from 'react';
import FavoriteLocationButton from '../favorite-locations/FavoriteLocationButton';
import { LocationListItem } from '../travellir-map/MapTypes';
import { View, StyleSheet } from 'react-native';

interface Props {
    item: LocationListItem
}

const styles = StyleSheet.create({
    buttonContainer: {
        width: 50,
        height: 50,
        marginTop: 5,
        marginBottom: -15,
        zIndex: 3,
        borderRadius: 30,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

class BigFavoriteLocationButton extends Component<Props> {
    render() {
        const { item } = this.props;
        return (
            <View style={styles.buttonContainer}>
                <FavoriteLocationButton big={true} item={item} />
            </View>
        )
    }
};

export default BigFavoriteLocationButton;