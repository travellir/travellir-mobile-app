import { LocationPage } from './location-page-types';
import Requests from '../http/Requests';
import TravellirStore from '../TravellirStore';
import { actions } from './location-page-redux';
import NavigatorService from '../navigation/NavigatorService';

export default {

    async fetchLocation(locationId: number) {
        const locationPage = await Requests.jsonGet(`/api/public/locations/${locationId}`);
        TravellirStore.dispatch(actions.locationPageFetched(locationPage))
    },

    async fetchDescription(locationId: number) {
        const locationPage = await Requests.jsonGet(`/api/public/locations/${locationId}/page`);
        TravellirStore.dispatch(actions.locationDescriptionFetched(locationPage))
    },

    openPage(locationId: number) {
        TravellirStore.dispatch(actions.requestLocationPage(locationId));
        NavigatorService.navigateTo('LocationPageScreen')
    }

}