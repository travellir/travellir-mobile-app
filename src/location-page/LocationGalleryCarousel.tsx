import React, { Component } from 'react';
import { Image, View, Text, Dimensions, StyleSheet } from 'react-native';
import { LocationPage } from './location-page-types';
import Carousel from 'react-native-snap-carousel';

interface Props {
    locationPage: LocationPage
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({

})

class LocationGalleryCarousel extends Component<Props> {

    render() {
        const { locationPage } = this.props;
        return (
            <Carousel
                loop={true}                
                data={this.galleryEntries()}
                renderItem={this.renderImage}
                sliderWidth={width}
                itemWidth={width}
            />
        );
    }

    galleryEntries = (): ReadonlyArray<string> => {
        const { locationPage } = this.props;
        return [locationPage.image]
            .concat(locationPage.gallery.images
                .map(image => image.preview))
    }

    renderImage = ({ item, index }: { item: string, index: number }): JSX.Element => {
        return (
            <Image source={{ uri: item }} style={{ height: 320, width }} />
        );
    }
};

export default LocationGalleryCarousel;