import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
import { IconToggle, Icon, RippleFeedback } from 'react-native-material-ui';
import { ListItem } from 'react-native-material-ui';


interface Props {
    onAssignmentsSelected: () => void,
    onSendPhotoSelected: () => void
}

class LocationPageMenu extends Component<Props> {

    state = {
        open: false
    }

    render() {
        return (
            <Menu
                opened={this.state.open}
                onBackdropPress={() => this.setState({ open: false })}>
                <MenuTrigger customStyles={{
                    triggerOuterWrapper: {
                        position: 'absolute',
                        top: 30,
                        right: 5,
                        width: 50,
                        height: 50,
                        zIndex: 10,
                        borderRadius: 30
                    }
                }}>
                    <IconToggle onPress={() => this.setState({ open: true })} color="#fff" name="more-vert" />
                </MenuTrigger>
                <MenuOptions>
                    <ListItem
                        onPress={() => this.selectItem(this.props.onAssignmentsSelected)}
                        divider
                        centerElement={{
                            primaryText: 'Dodaj obiekt do tras',
                        }}
                    />
                    <ListItem
                        onPress={() => this.selectItem(this.props.onSendPhotoSelected)}
                        divider
                        centerElement={{
                            primaryText: 'Prześlij zdjęcie obiektu',
                        }}
                    />
                </MenuOptions>
            </Menu>
        );
    }

    selectItem = (func: () => void) => {
        this.setState({
            open: false
        });
        func();
    }
};

export default LocationPageMenu