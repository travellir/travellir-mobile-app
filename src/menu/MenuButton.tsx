import React, { Component } from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { IconToggle } from 'react-native-material-ui';
import DrawerService from '../travellir-drawer/DrawerService';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    buttonContainer: {
        position: 'absolute',
        top: 30,
        left: 10,
        width: 60,
        height: 60,
        zIndex: 10,
    }
})

class MenuButton extends Component {
    render() {
        return (
            <View style={styles.buttonContainer}>
                <IconToggle onPress={DrawerService.toggleDrawer} name="menu" color='#fff' />
            </View>
        )
    }
};

export default MenuButton;