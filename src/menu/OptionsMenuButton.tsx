import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { IconToggle } from 'react-native-material-ui';

interface OptionsMenuButtonProps {
    onPress: () => void
}

class OptionsMenuButton extends React.PureComponent<OptionsMenuButtonProps> {
    render() {
        const { onPress } = this.props;
        return (
            <IconToggle onPress={onPress} color="#fff" name="more-vert" />
        );
    }
}

export default OptionsMenuButton;