import React, { Component } from 'react';
import { Menu, MenuTrigger, MenuOptions } from 'react-native-popup-menu';
import OptionsMenuButton from './OptionsMenuButton';
import { StyleSheet } from 'react-native';
import { ListItem, Icon } from 'react-native-material-ui';

interface State {
    open: boolean
}

const styles = StyleSheet.create({
    triggerOuterWrapper: {
        position: 'absolute',
        top: 30,
        right: 5,
        width: 50,
        height: 50,
        zIndex: 10,
        borderRadius: 30
    }
})


class OptionsMenu extends Component<any, State> {

    state = {
        open: false
    }

    render() {
        const { open } = this.props;
        return (
            <Menu
                opened={this.state.open}
                onBackdropPress={() => this.setState({ open: false })}>
                <MenuTrigger
                    customStyles={{ triggerOuterWrapper: styles.triggerOuterWrapper }}>
                    <OptionsMenuButton onPress={() => this.setState({ open: true })} />
                </MenuTrigger>
                <MenuOptions>
                    {this.props.children}
                </MenuOptions>
            </Menu>
        );
    }
};

interface MenuOptionProps {
    text?: string,
    icon?: string,
    divider?: boolean
    onPress?: () => void
}
export class MenuOption extends React.Component<MenuOptionProps> {
    render() {
        const { text, icon, divider, onPress } = this.props;
        return (

            <ListItem
                dense={true}
                onPress={onPress}
                divider={divider}
                leftElement={icon ? <Icon name={icon} /> : undefined}
                centerElement={text ? { primaryText: text, } : ''}
            />)
    }
}

export default OptionsMenu;