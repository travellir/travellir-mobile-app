import { actions } from '../travellir-map-redux';
import TravellirStore from '../../TravellirStore';

export default {

    closeCallout() {
        TravellirStore.dispatch(actions.closeCallout());
    },
    
    toggleCallout(locationId: number) {
        TravellirStore.dispatch(actions.toggleCallout(locationId));
    },
}