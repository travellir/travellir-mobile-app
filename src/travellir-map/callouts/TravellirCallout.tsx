import React, { Component } from 'react';
import { Image, View, Text, Dimensions, TouchableOpacity } from 'react-native';
import CalloutService from './CalloutService';
import { LocationListItem } from '../MapTypes';
import { StyleSheet } from 'react-native';
import FavoriteLocationButton from '../../favorite-locations/FavoriteLocationButton';
import { IconToggle, Button, RippleFeedback } from 'react-native-material-ui';
import MarkersService from '../MarkersService';
import LocationPageService from '../../location-page/LocationPageService';

const { width, height } = Dimensions.get('window');
const cardWidth = width * .8;

interface Props {
    locationItem: LocationListItem
}

const styles = StyleSheet.create({
    card: {
        left: 5,
        marginBottom: 5,
        zIndex: 2,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    bottomPart: {
        top: 0,
        width: cardWidth,
        flexDirection: 'column',
    },
    cardTitle: {
        borderTopLeftRadius: 2,
        borderTopRightRadius: 2,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    stickBottom: {
        width: cardWidth,
        position: 'absolute',
        bottom: 0,
    },
    stickTop: {
        width: cardWidth,
        position: 'absolute',
        top: 0,
    },
    buttonContainer: {
        backgroundColor: 'rgba(0, 0, 0, .7)'
    },
    buttonText: {
        color: '#fff'
    },
    cardActions: {
        flexDirection: 'column',
        alignItems: 'center'
    }
})

class TravellirCallout extends Component<Props> {
    render() {
        const { locationItem } = this.props;
        const coords = {
            latitude: locationItem.coords.lat,
            longitude: locationItem.coords.lng
        };
        return (
            <View style={styles.card} >
                <View style={styles.bottomPart}>
                    <View style={styles.cardTitle}>
                        <View style={{ padding: 10, width: cardWidth * .87, flexDirection: 'column' }}>
                            <Text style={{ fontSize: 16 }} numberOfLines={1}>{locationItem.name}</Text>
                            <Text style={{ color: '#303030', fontSize: 14 }} numberOfLines={1}>{locationItem.address}</Text>
                        </View>
                        <View>
                            <IconToggle onPress={CalloutService.closeCallout}
                                name="clear"
                                color="#000"
                                size={20} />
                        </View>
                    </View>
                </View>
                <View>
                    <TouchableOpacity activeOpacity={.9} onPress={this.openLocationPage}>
                        <Image
                            borderRadius={2}
                            source={{ uri: locationItem.image }}
                            style={{ width: cardWidth, height: 160 }} />
                    </TouchableOpacity>
                    <View style={[styles.stickTop, { justifyContent: 'flex-end', flexDirection: 'row' }]}>
                        <View style={[styles.cardActions, { width: 40 }]}>
                            <FavoriteLocationButton item={locationItem} />
                        </View>
                    </View>
                    <View style={[styles.stickBottom, { flexDirection: 'column' }]}>
                        <Image source={{ uri: locationItem.icon }} style={{ width: 60, height: 60 }} />
                        <Button text="Pokaż na mapie" onPress={() => MarkersService.panToLocation(locationItem)} style={{ container: styles.buttonContainer, text: styles.buttonText }} />
                    </View>
                </View>
            </View>
        )
    }

    openLocationPage = () => {
        const { locationItem } = this.props;
        LocationPageService.openPage(locationItem.id);
    }
};

export default TravellirCallout;