import { CalloutState } from '../travellir-map-redux';
import { LocationListItem } from '../MapTypes';

export namespace CalloutStates {
    export const CALLOUT_CLOSED = {
        isOpen: false,
        loaded: false
    } as CalloutState;

    export const locationCalloutOpen = (locationItem: LocationListItem) => ({
        locationItem,
        isOpen: true,
        loaded: false
    }) as CalloutState;
}