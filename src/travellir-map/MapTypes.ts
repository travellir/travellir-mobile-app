export interface Region {
    latitude: number,
    longitude: number,
    latitudeDelta: number
    longitudeDelta: number
};

export interface TravellirBounds {
    ne: Coords,
    sw: Coords,
}

export interface LocationListItem {
    address: string,
    coords: Coords,
    icon: string,
    icon_id: number,
    id: number,
    image: string,
    image_id: string,
    name: string,
    zoom: number
}

export interface FetchedMap {
    locations: Array<Location>,
    clusters: Array<Cluster>,
    marker_list: {
        next_page?: string,
        markers: Array<LocationListItem>
    }
}

export interface TravellirRegion extends TravellirBounds {
    zoom: number
}

export interface LoadedImages {
    [key: number]: boolean
}

export interface Coords {
    lat: number,
    lng: number
}

export interface Cluster {
    size: number,
    coords: Coords
}

export interface Location {
    id: number,
    icon_thumbnail: string,
    coords: Coords
}