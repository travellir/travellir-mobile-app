import React, { Component } from 'react';
import { Marker } from 'react-native-maps';
import { Platform } from 'react-native';
import MarkerIcon from './MarkerIcon';
import { Location, LoadedImages } from './MapTypes';
import MarkersService from './MarkersService';
import { connectState } from '../travellir-redux';
import { TravellirMapState } from './travellir-map-redux';
import CalloutService from './callouts/CalloutService';

export function locationIconKey(location: Location, loadedImages: LoadedImages): string {
    if (Platform.OS === 'ios')
        return `marker-${location.id}`;
    return `marker-${location.id}:${location.id in loadedImages}`;
}

interface Props extends TravellirMapState {
    location: Location,
}

class MapMarker extends Component<Props> {

    render() {
        const { location } = this.props;

        return (
            <Marker
                onPress={this.onMarkerPress}
                coordinate={{
                    latitude: location.coords.lat,
                    longitude: location.coords.lng
                }}>
                <MarkerIcon
                    onImageLoaded={this.onImageLoaded}
                    location={location} />
            </Marker>
        );
    }

    onMarkerPress = () => {
        const { location } = this.props;
        CalloutService.toggleCallout(location.id);
    }

    onImageLoaded = () => {
        const { location } = this.props;
        MarkersService.markAsLoaded(location);
    }
}

export default connectState(MapMarker);