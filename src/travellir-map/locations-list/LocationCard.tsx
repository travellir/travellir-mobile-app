import React, { Component } from 'react';
import { LocationListItem } from '../MapTypes';
import { View, Text, Image, Dimensions, StyleSheet } from 'react-native';
import NavigatorService from '../../navigation/NavigatorService';
import FavoriteLocationButton from '../../favorite-locations/FavoriteLocationButton';
import ShowLocationButton from './ShowLocationButton';
import CalloutService from '../callouts/CalloutService';

const { width, height } = Dimensions.get('window');

const itemWidth = width - 10;

interface Props {
    item: LocationListItem
}

const cardBorderdRadius = 3;

const styles = StyleSheet.create({
    cardContainer: {
        marginVertical: 4,
        width: itemWidth,
        borderRadius: cardBorderdRadius
    },
    iconContainer: {
        position: 'absolute',
        width: 60,
        bottom: 85,
        right: 5,
        zIndex: 1,
        alignItems: 'center'
    },
    cardImage: {
        height: 250,
        borderRadius: cardBorderdRadius
    },
    icon: {
        width: 55,
        height: 60
    },
    cardTitleContainer: {
        borderBottomLeftRadius: cardBorderdRadius,
        borderBottomRightRadius: cardBorderdRadius,
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, .8)',
        width: itemWidth,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 80
    },
    cardTitle: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 15,
        width: width * .8
    },
    primaryTitle: {
        color: '#f0f0f0',
        fontSize: 20,
        marginBottom: 5,
        textAlign: 'left'
    },
    secondaryTitle: {
        color: '#cdcdcd',
        fontSize: 14,
        textAlign: 'left'
    },
    actionsContainer: {
        flexDirection: 'column',
        alignContent: 'flex-start',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 8,
        paddingTop: 2
    }
})

class LocationCard extends Component<Props> {

    render() {
        const { item } = this.props;
        return (
            <View style={styles.cardContainer}>
                <Image style={styles.cardImage} source={{ uri: item.image }} />

                <View style={styles.iconContainer}>
                    <Image source={{ uri: item.icon }} style={styles.icon} />
                </View>

                <View style={styles.cardTitleContainer}>
                    <View style={styles.cardTitle}>
                        <Text numberOfLines={1} style={styles.primaryTitle}>{item.name}</Text>
                        <Text numberOfLines={1} style={styles.secondaryTitle}>{item.address}</Text>
                    </View>
                    <CardActions item={item} />
                </View>
            </View>
        );
    }
}

class CardActions extends React.Component<Props> {

    render() {
        const { item } = this.props;
        return (
            <View style={styles.actionsContainer}>
                <FavoriteLocationButton item={item} />
                <ShowLocationButton item={item} onPress={this.onLocationShow} />
            </View>
        )
    }

    onLocationShow = () => {
        const { item } = this.props;
        CalloutService.toggleCallout(item.id);
        NavigatorService.goBack();
    }

}

export default LocationCard;