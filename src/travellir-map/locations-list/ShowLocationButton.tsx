import React, { Component } from 'react';
import { IconToggle } from 'react-native-material-ui';
import { LocationListItem } from '../MapTypes';
import { StyleSheet } from 'react-native';
import MarkersService from '../MarkersService';

interface Props {
    item: LocationListItem
    onPress?: () => void
}

const styles = StyleSheet.create({
    showOnMapContainer: {
        backgroundColor: '#FF5722',
        borderRadius: 50,
        width: 27,
        height: 27
    }
})

class ShowLocationButton extends Component<Props> {
    render() {
        return (
            <IconToggle
                name="map"
                onPress={this.showLocationOnMap}
                style={{ container: styles.showOnMapContainer }}
                size={18}
                color='#fff' />
        )
    }
    showLocationOnMap = () => {
        const { item, onPress } = this.props;
        MarkersService.panToLocation(item);
        if (onPress)
            onPress();
    }
};

export default ShowLocationButton;