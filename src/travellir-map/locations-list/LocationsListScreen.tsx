import React, { Component } from 'react';
import { connectState } from '../../travellir-redux';
import { View, FlatList, Dimensions } from 'react-native';
import { TravellirMapState } from '../travellir-map-redux';
import { LocationListItem } from '../MapTypes';
import LocationCard from './LocationCard';
import TravellirBottomNavigation from '../TravellirBottomNavigation';
import { NavigationInjectedProps } from 'react-navigation';

const { height } = Dimensions.get('window');

interface Props extends TravellirMapState { }

class LocationsListScreen extends Component<Props> {
    render() {
        const { viewData } = this.props;
        return (
            <View style={{
                paddingTop: 5,
                minHeight: height,
                backgroundColor: '#4F4F4F'
            }}>
                <FlatList
                    style={{
                        marginBottom: 60
                    }}
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    data={viewData.marker_list.markers}
                    keyExtractor={(item, index) => `list-item-${item.id}`}
                    renderItem={(entry) => <LocationCard item={entry.item} key={entry.index} />} />
                <View style={{ position: 'absolute', bottom: 0, width: 360 }}>
                    <TravellirBottomNavigation activeTile='locations-list' />
                </View>
            </View>
        );
    }
}

export default connectState(LocationsListScreen);