import React, { Component } from 'react'
import { Dimensions, StyleSheet } from 'react-native';
import TravellirCallout from './callouts/TravellirCallout';
import { connectState } from '../travellir-redux';
import { TravellirMapState } from './travellir-map-redux';
import { View } from 'react-native';
import TravellirBottomNavigation from './TravellirBottomNavigation';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    footer: {
        bottom: 0,
        width,
        zIndex: 1,
        elevation: 1,
        position: 'absolute',
        backgroundColor: 'transparent'
    },
});

class MapFooter extends Component<TravellirMapState> {
    render () {
        return (
            <View style={styles.footer}>
                {this.renderCallout()}
                <TravellirBottomNavigation activeTile='map' />
            </View>
        )
    }

    renderCallout = () => {
        const { calloutState } = this.props;
        if (!calloutState.locationItem)
            return;

        return (<TravellirCallout locationItem={calloutState.locationItem} />)
    }
}

export default connectState(MapFooter)