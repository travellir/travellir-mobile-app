import { TravellirRegion, FetchedMap, TravellirBounds, Location, Coords, LocationListItem } from './MapTypes';
import { Region } from 'react-native-maps';
import geoViewport from '@mapbox/geo-viewport';
import TravellirStore from '../TravellirStore';
import { actions } from './travellir-map-redux';
import { Dimensions } from 'react-native';
import { UserRoute } from '../user-routes/user-routes-types';

const dimensions = Dimensions.get('window');

export default {

    clearRegionRequest() {
        TravellirStore.dispatch(actions.clearRegionRequest())
    },
    panToLocation(locationItem: LocationListItem) {
        const { coords, zoom } = locationItem;
        const bounds = geoViewport.bounds([coords.lat, coords.lng], zoom, [dimensions.width, dimensions.height]);

        const travellirBounds = {
            ne: {
                lat: bounds[0],
                lng: bounds[1]
            },
            sw: {
                lat: bounds[2],
                lng: bounds[3]
            }
        }

        const latitudeDelta = Math.abs(travellirBounds.ne.lat - travellirBounds.sw.lat) / 2;
        const longitudeDelta = Math.abs(travellirBounds.ne.lng - travellirBounds.sw.lng) / 2;

        const travellirRegion = {
            latitude: coords.lat,
            longitude: coords.lng,
            latitudeDelta,
            longitudeDelta
        };
        TravellirStore.dispatch(actions.panToRegion(travellirRegion))
    },

    panToCoords(coords: Coords, zoom: number) {
        const bounds = geoViewport.bounds([coords.lat, coords.lng], zoom, [dimensions.width, dimensions.height]);

        const travellirBounds = {
            ne: {
                lat: bounds[0],
                lng: bounds[1]
            },
            sw: {
                lat: bounds[2],
                lng: bounds[3]
            }
        }

        const latitudeDelta = Math.abs(travellirBounds.ne.lat - travellirBounds.sw.lat) / 2;
        const longitudeDelta = Math.abs(travellirBounds.ne.lng - travellirBounds.sw.lng) / 2;

        const travellirRegion = {
            latitude: coords.lat,
            longitude: coords.lng,
            latitudeDelta,
            longitudeDelta
        };

        TravellirStore.dispatch(actions.panToRegion(travellirRegion))
    },

    saveRegion(region: Region) {
        TravellirStore.dispatch(actions.saveRegion(region))
    },

    asTravellirBounds(region: Region): TravellirBounds {
        const latituteDelta = (region.latitudeDelta / 2);
        const longitudeDelta = (region.longitudeDelta / 2);
        return {
            ne: {
                lat: region.latitude + latituteDelta,
                lng: region.longitude + longitudeDelta,
            },
            sw: {
                lat: region.latitude - latituteDelta,
                lng: region.longitude - longitudeDelta,
            }
        };
    },

    markAsLoaded(location: Location): void {
        TravellirStore.dispatch(actions.imageLoaded(location.id))
    },

    mapZoom(bounds: TravellirBounds, dimensions: { width: number, height: number }): number {
        const viewport = geoViewport.viewport(
            [bounds.sw.lng, bounds.sw.lat, bounds.ne.lng, bounds.ne.lat],
            [dimensions.width, dimensions.height]
        );
        return viewport.zoom;
    },

    async fetchCustomRoute(region: TravellirRegion, route: UserRoute) {
        const queryParams = route.locationIds.map(id => `locationIds=${id}`).join('&');
        const response = await fetch(`https://travellir.leers.pl/api/public/custom-map?includeIcons=true&${queryParams}`, {
            method: 'POST',
            body: JSON.stringify(region),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Failed to fetch locatoons');
        }
        const fetchedMap: FetchedMap = await response.json();
        TravellirStore.dispatch(actions.mapViewChanged(fetchedMap));
    },

    async fetchMarkers(region: TravellirRegion) {
        const response = await fetch('https://travellir.leers.pl/api/list-locations?includeIcons=true', {
            method: 'POST',
            body: JSON.stringify(region),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Failed to fetch locatoons');
        }
        const fetchedMap: FetchedMap = await response.json();
        TravellirStore.dispatch(actions.mapViewChanged(fetchedMap));
    }
}