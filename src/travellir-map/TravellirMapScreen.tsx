import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Text, ToastAndroid } from "react-native";
import MapView from "react-native-maps";
import MapMarker, { locationIconKey } from './MapMarker';
import { Location, Region, TravellirRegion } from './MapTypes';
import MarkersService from './MarkersService';
import MapCluster, { clusterKey } from './MapCluster';
import SearchBar from './SearchBar';
import { TravellirMapState, initialMapCenter } from './travellir-map-redux';
import { connectState } from '../travellir-redux';
import DrawerService from '../travellir-drawer/DrawerService';
import TravellirDrawer from '../travellir-drawer/TravellirDrawer';
import MapFooter from './MapFooter';
import { LocalizationState } from '../localization/localization-redux';
import LocalizationService from '../localization/LocalizationService';
import CurrentPositionButton from './CurrentPositionButton';
import CurrenPositionMarker from './CurrenPositionMarker';

const { width, height } = Dimensions.get('window');

interface Props extends TravellirMapState, LocalizationState { }

class TravellirMapScreen extends Component<Props, any> {

    private _mapView: MapView | null = null;

    render() {
        const { region, viewData, loadedImages, calloutState } = this.props;
        return (
            <View style={styles.mapContainer}>
                <TravellirDrawer />
                <SearchBar />
                <MapView
                    ref={_ref => { this._mapView = _ref }}
                    moveOnMarkerPress={false}
                    provider={'google'}
                    style={styles.map}
                    initialRegion={initialMapCenter}
                    onRegionChangeComplete={this.loadNewMarkers}>
                    {
                        viewData.locations
                            .map((location, i) =>
                                <MapMarker
                                    key={locationIconKey(location, loadedImages)}
                                    location={location} />)
                    }
                    {
                        viewData.clusters
                            .map((cluster, i) =>
                                <MapCluster
                                    key={clusterKey(cluster)}
                                    cluster={cluster} />)
                    }
                    <CurrenPositionMarker />
                </MapView>
                <CurrentPositionButton />
                <MapFooter />
            </View>
        );
    }

    componentDidUpdate(prevProps: LocalizationState & TravellirMapState) {
        const { permissionGranted, requestedRegion } = this.props;
        if (!prevProps.permissionGranted && permissionGranted) {
            LocalizationService.fetchCurrentPosition();
        }
        if (!prevProps.requestedRegion && requestedRegion && this._mapView) {
            this._mapView.animateToRegion(requestedRegion);
        }
    }

    loadNewMarkers = (region: Region) => {
        const { requestedRegion } = this.props;

        MarkersService.saveRegion(region);
        this.reloadMarkers(region);

        if (requestedRegion && this._mapView) {
            this._mapView.animateToRegion(requestedRegion);
            MarkersService.clearRegionRequest();
        }
    }

    reloadMarkers = (region: Region) => {
        const bounds = MarkersService.asTravellirBounds(region);
        const currentZoom = MarkersService.mapZoom(bounds, { width, height })

        const travellirRegion: TravellirRegion = {
            ...bounds,
            zoom: currentZoom
        };

        MarkersService.fetchMarkers(travellirRegion);
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#1976D2",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 3,
        width: "100%"
    },
    mapContainer: {
        flex: 1
    },
    map: {
        flex: 1
    }
});

export default connectState(TravellirMapScreen);
