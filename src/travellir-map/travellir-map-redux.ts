
import { Region } from 'react-native-maps';
import { Cluster, LoadedImages, FetchedMap, Location, LocationListItem, Coords } from './MapTypes';
import { RawAction, TravellirModule } from '../travellir-redux';
import { CalloutStates } from './callouts/CalloutStates';

export interface CalloutState {
    isOpen: boolean,
    locationItem?: LocationListItem,
}

export interface TravellirMapState {
    requestedRegion: Region | null,
    calloutState: CalloutState,
    region: Region,
    viewData: FetchedMap,
    loadedImages: LoadedImages
};

export const initialMapCenter = {
    latitude: 51.78604538966107,
    longitude: 21.000737529993057,
    latitudeDelta: 2.8888992080406055,
    longitudeDelta: 2.115514948964119
};

interface MapPoint {
    coords: Coords,
    zoom: number,
    dimensions: { width: number, height: number }
}

export const types = {
    CLEAR_REGION_CHANGE: 'CLEAR_REGION_CHANGE',
    REGION_CHANGE_REQUESTED: 'REGION_CHANGE_REQUESTED',
    TOGGLE_CALLOUT: 'TOGGLE_CALLOUT',
    IMAGE_LOADED: 'IMAGE_LOADED',
    MAP_VIEW_CHANGED: 'MAP_VIEW_CHANGED',
    REGION_CHANGED: 'REGION_CHANGED',
    CALLOUT_CLOSE: 'CALLOUT_CLOSE'
}

export const actions = {
    clearRegionRequest(): RawAction<number> {
        return {
            type: types.CLEAR_REGION_CHANGE,
            payload: -1
        }
    },
    panToRegion(region: Region): RawAction<Region> {
        return {
            type: types.REGION_CHANGE_REQUESTED,
            payload: region
        }
    },
    closeCallout(): RawAction<number> {
        return {
            type: types.CALLOUT_CLOSE,
            payload: -1
        }
    },
    toggleCallout(locationId: number): RawAction<number> {
        return {
            type: types.TOGGLE_CALLOUT,
            payload: locationId
        }
    },
    saveRegion(region: Region): RawAction<Region> {
        return {
            type: types.REGION_CHANGED,
            payload: region
        }
    },
    imageLoaded(locationId: number): RawAction<number> {
        return {
            type: types.IMAGE_LOADED,
            payload: locationId
        }
    },
    mapViewChanged(fetchedMap: FetchedMap): RawAction<FetchedMap> {
        return {
            type: types.MAP_VIEW_CHANGED,
            payload: fetchedMap
        }
    }
}

export const TravellirMapModule = {
    initialState: {
        requestedRegion: null,
        calloutState: CalloutStates.CALLOUT_CLOSED,
        region: initialMapCenter,
        viewData: {
            locations: [],
            clusters: [],
            marker_list: {
                markers: []
            }
        },
        loadedImages: {}
    },
    reducers: {
        [types.CLEAR_REGION_CHANGE](state: TravellirMapState) {
            return {
                requestedRegion: null
            }
        },
        [types.REGION_CHANGE_REQUESTED](state: TravellirMapState, region: Region) {
            const latitudeDelta = Math.min(region.latitudeDelta, state.region.latitudeDelta);
            const longitudeDelta = Math.min(region.longitudeDelta, state.region.longitudeDelta);

            const destinationRegion: Region = {
                latitude: region.latitude,
                longitude: region.longitude,
                latitudeDelta,
                longitudeDelta
            }
            return {
                requestedRegion: destinationRegion
            }
        },
        [types.TOGGLE_CALLOUT](state: TravellirMapState, locationId: number) {
            if (state.calloutState.isOpen && state.calloutState.locationItem) {
                if (locationId === state.calloutState.locationItem.id)
                    return { calloutState: CalloutStates.CALLOUT_CLOSED }
            }
            const listItem = state.viewData.marker_list.markers
                .find(locationItem => locationItem.id === locationId)
            const calloutState = listItem ? CalloutStates.locationCalloutOpen(listItem) : CalloutStates.CALLOUT_CLOSED;
            return {
                calloutState
            }
        },
        [types.CALLOUT_CLOSE](state: TravellirMapState, locationId: number) {
            return {
                calloutState: CalloutStates.CALLOUT_CLOSED
            }
        },
        [types.MAP_VIEW_CHANGED](state: TravellirMapState, payload: FetchedMap) {
            return {
                viewData: payload
            };
        },
        [types.IMAGE_LOADED](state: TravellirMapState, locationId: number) {
            const loadedImages: LoadedImages = Object.assign({}, state.loadedImages, { [locationId]: true })
            return {
                loadedImages
            }
        },
        [types.REGION_CHANGED](state: any, payload: Region) {
            return {
                region: payload
            }
        }
    }
} as TravellirModule<TravellirMapState>;
