import * as React from 'react';
import { Icon, RippleFeedback } from 'react-native-material-ui';
import { Text, Dimensions, View, StyleSheet, TextInput, TouchableWithoutFeedback } from 'react-native';
import DrawerService from '../../travellir-drawer/DrawerService';
import NavigatorService from '../../navigation/NavigatorService';
import { COLOR } from 'react-native-material-ui';
import { UserRoute } from '../../user-routes/user-routes-types';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    statusBar: {
        position: 'absolute',
        top: 25,
        zIndex: 1,
        width: width,
    },
    card: {
        backgroundColor: '#fff',
        shadowColor: '#303030',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 6
    }
})


interface Props {
    route: UserRoute
}

class CustomMapHeader extends React.Component<Props, any> {
    render() {
        const { route } = this.props;
        return (
            <View style={[styles.statusBar, styles.card]}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                    {this.menuIcon()}
                    <View style={{ width: 300, padding: 5 }}>
                        <Text style={{ color: COLOR.grey800, fontSize: 16 }}>Trasa <Text style={{ fontWeight: 'bold' }}>{route.name}</Text></Text>
                    </View>
                </View>
            </View>
        );
    }

    private menuIcon() {
        return (<View style={{ borderRadius: 25, marginRight: 15, overflow: 'hidden' }}>
            <RippleFeedback borderless={false} onPress={DrawerService.toggleDrawer}>
                <View style={{ height: 33, width: 33, borderRadius: 25, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name='menu' size={25} color='#404040' />
                </View>
            </RippleFeedback>
        </View>);
    }
}
export default CustomMapHeader;