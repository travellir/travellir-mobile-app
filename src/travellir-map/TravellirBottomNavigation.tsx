import React, { Component } from 'react';
import { BottomNavigation } from 'react-native-material-ui';
import { View, StyleSheet, Dimensions } from 'react-native';
import { NavigationInjectedProps } from 'react-navigation';
import { connectState } from '../travellir-redux';
import NavigatorService from '../navigation/NavigatorService';

interface Props {
    activeTile: 'map' | 'locations-list'
}

class TravellirBottomNavigation extends Component<Props> {

    render() {
        const { activeTile } = this.props;
        return (
            <BottomNavigation active={activeTile} hidden={false} >
                <BottomNavigation.Action
                    disabled={activeTile === 'map'}
                    active={activeTile !== 'map'}
                    key='map'
                    icon="map"
                    label="Mapa"
                    onPress={() => {
                        if (activeTile !== 'map')
                            NavigatorService.goBack()
                    }}
                />
                <BottomNavigation.Action
                    disabled={activeTile === 'locations-list'}
                    active={activeTile !== 'locations-list'}
                    icon="list"
                    key='locations-list'
                    label="Lista obiektów"
                    onPress={() => {
                        if (activeTile !== 'locations-list')
                            NavigatorService.navigateTo('LocationsListScreen')
                    }}
                />
            </BottomNavigation>
        );
    }
}

export default TravellirBottomNavigation;