import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { connectState } from '../travellir-redux';
import { Marker } from 'react-native-maps';
import { LocalizationState } from '../localization/localization-redux';
import { COLOR } from 'react-native-material-ui';

interface Props {
}

const styles = StyleSheet.create({
    currentPosition: {
        width: 13,
        height: 13,
        borderRadius: 10,
        backgroundColor: COLOR.blue200,
        borderWidth: 2,
        borderColor: COLOR.blue500,
    }
})

class CurrenPositionMarker extends React.PureComponent<Props & LocalizationState, any> {
    render() {
        const { currentLocation } = this.props;
        if (!currentLocation)
            return null;
            
        return (
            <Marker
                coordinate={{
                    latitude: currentLocation.lat,
                    longitude: currentLocation.lng
                }}>
                <View style={styles.currentPosition} />
            </Marker>
        );
    }
}

export default connectState(CurrenPositionMarker);