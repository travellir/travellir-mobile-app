import * as React from 'react';
import { View, StyleSheet, Text, } from 'react-native';
import { ActionButton, COLOR } from 'react-native-material-ui';
import { connectState } from '../travellir-redux';
import { LocalizationState } from '../localization/localization-redux';
import MarkersService from './MarkersService';

interface Props {
}

class CurrentPositionButton extends React.PureComponent<Props & LocalizationState, any> {
    render() {
        const { currentLocation } = this.props;
        if (!currentLocation)
            return null;

        return (
            <ActionButton
                onPress={this.panToLocation}
                icon={'my-location'}
                style={{
                    positionContainer: {
                        zIndex: 3,
                        elevation: 3,
                        bottom: 65,
                        right: 10
                    },
                    container: {
                        width: 50,
                        height: 50,
                        backgroundColor: COLOR.blue600
                    }
                }}
            />
        );
    }

    panToLocation = () => {
        const { currentLocation } = this.props;
        if (!currentLocation)
            return;

        MarkersService.panToCoords(currentLocation, 11);
    }
}
export default connectState(CurrentPositionButton);