import React, { Component } from 'react';
import { Image } from 'react-native';
import { Location } from './MapTypes';

const TravellirLogo = require('../../assets/travellir-logo.png');

export interface MarkerViewProps {
    location: Location,
    onImageLoaded: () => void
}

class MarkerIcon extends Component<MarkerViewProps> {

    render() {
        const { location, onImageLoaded } = this.props;
        return (
            <Image
                onLoadEnd={onImageLoaded}
                source={{ uri: location.icon_thumbnail }} 
                style={{ width: 50, height: 50 }} />
        );
    }
}

export default MarkerIcon;