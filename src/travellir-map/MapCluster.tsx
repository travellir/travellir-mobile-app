import React, { Component } from 'react';
import { Marker } from 'react-native-maps';
import { View, Text } from 'react-native';
import { Cluster } from './MapTypes'

interface Props {
    cluster: Cluster
}

export function clusterKey(cluster: Cluster): string {
    return `cluster-${cluster.coords.lat}-${cluster.coords.lng}-${cluster.size}`
}

class LocationsCluster extends Component<Props> {
    render() {
        const { cluster } = this.props;
        return (
            <Marker
                coordinate={{
                    latitude: cluster.coords.lat,
                    longitude: cluster.coords.lng
                }}>
                <View
                    style={{
                        width: cluster.size === 1 ? 10 : 23,
                        height: cluster.size === 1 ? 10 : 23,
                        backgroundColor: "#fff",
                        borderRadius: 17,
                        borderStyle: "solid",
                        borderWidth: cluster.size === 1 ? 1 : 2,
                        borderColor: "#000000"
                    }}>
                    {cluster.size > 1 ? <Text
                        style={{
                            color: "#000000",
                            fontWeight: 'bold',
                            textAlign: "center", textAlignVertical: 'center', lineHeight: 20
                        }}>
                        {cluster.size}
                    </Text> : null}
                </View>
            </Marker>
        );
    }
}

export default LocationsCluster;