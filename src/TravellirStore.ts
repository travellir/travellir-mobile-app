import { createStore, combineReducers } from "redux";
import { navigationReducer } from './navigation/navigation-reducer'
import { travellirReducer } from './redux-registry';

const reducer = combineReducers({
    navigation: navigationReducer,
    travellir: travellirReducer
})

const TravellirStore = createStore(reducer);

export default TravellirStore;