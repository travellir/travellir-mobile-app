import React, { Component } from 'react'
import { COLOR, ThemeProvider } from 'react-native-material-ui';
import TravellirStore from './src/TravellirStore';
import { Provider } from 'react-redux';
import RootScreen from './src/RootScreen';

const uiTheme = {
    palette: {
        primaryColor: COLOR.green500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    bottomNavigationAction: {
        iconActive: {
            color: '#FF5722',
        },
        labelActive: {
            color: '#FF5722',
        }
    }
};

class App extends Component {
    render() {
        return (
            <ThemeProvider uiTheme={uiTheme}>
                <Provider store={TravellirStore} >
                    <RootScreen />
                </Provider>
            </ThemeProvider>
        )
    }
}



export default App